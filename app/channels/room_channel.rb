class RoomChannel < ApplicationCable::Channel
    def subscribed
      rental = Rental.find params[:rental]
      stream_for rental
  
      # or
      # stream_from "room_#{params[:room]}"
    end
  end
  