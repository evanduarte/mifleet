class ApplicationController < ActionController::Base
    
    include Pundit
    before_action :configure_permitted_parameters,
                if: :devise_controller?

    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
    rescue_from ActionController::InvalidAuthenticityToken,
              with: :invalid_auth_token
    protect_from_forgery with: :exception, unless: :json_request?
    protect_from_forgery with: :null_session, if: :json_request?
    skip_before_action :verify_authenticity_token, if: :json_request?

    before_action :set_current_user, if: :json_request?    
    


 
    
    private  
    def json_request?
        request.format.json?
    end  
    
    # Use api_user Devise scope for JSON access
    def authenticate_user!(*args)
        super and return unless args.blank?
        json_request? ? authenticate_api_user! : super
    end  
    
    def invalid_auth_token
        respond_to do |format|
        format.html { redirect_to sign_in_path, 
                        error: 'Login invalid or expired' }
        format.json { head 401 }
        end
    end 
    
    # So we can use Pundit policies for api_users
    def set_current_user
        @current_user ||= warden.authenticate(scope: :api_user)
    end
    
    def user_not_authorized
        flash[:alert] = "Tu n'es pas autorisé à cette action!"
        redirect_to root_path
    end
    
    

    protected
    
    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:avatar, :name, :firstname, :phone])
        devise_parameter_sanitizer.permit(:account_update, keys: [:avatar, :name, :firstname, :phone])
    end
end
