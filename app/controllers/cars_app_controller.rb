class CarsAppController < ApplicationController

    before_action :set_car, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user!, :except => [:show, :index]
  
   
    def index
      @cars = Car.all.order :name

      @result = []
      @result_image = []
      @most_rent = []

      @cars.each do |car|
        @result << car
        @result_image << url_for(car.cars_thumbnail.first)
        @most_rent << Rental.where(car_id: car.id).count

      end

    
      



      #@result = @result.sort_by(&:distance)

      # ------------------------ REACT JS ---------------------------------


      gon.results_all_cars = @result
      gon.results_image_all_cars = @result_image
      gon.most_rent = @most_rent

      if user_signed_in?
        gon.current_user_id = current_user.id
      else  
        gon.current_user_id = nil
      end

      # if user_signed_in?
      #   @ip_address= request.location
      #   user_location = Geocoder.search(@ip_address.ip).first.coordinates
      #   gon.cars_around = Car.all.near(user_location,20, units: :km)
      # end
      
      
      

    end
  
    def show
 
  
      @users = User.all
 
      @hash = Gmaps4rails.build_markers(@car) do |car, marker|
        marker.lat car.latitude
        marker.lng car.longitude
      end

      @rental = Rental.new

  
      @invalidDates = Rental.all.where(car_id: @car.id)
      gon.invalidDates=@invalidDates
      session[:from]=params[:from]

      gon.date_start = Date.today
      gon.date_end = Date.today + 1
      @time_start = ['7:30', '7:30:00']
      @time_end = ['10:00', '10:00:00']
      @distance_rental = ['Distance',0]

      

    end
  
    def new
        @car = Car.new
        
    end
  
    def edit

    end
  
    def create   
        @car = current_user.cars.new(car_params)
        authorize @car
        if @car.save
            #redirect_to new_car_rental_path(@car)
            flash[:notice] = "Le vehicule #{@car.name} a été créé avec succès!"
            redirect_to cars_app_index_path
        else
            render :new
        end
    end
  
    def update
        
        authorize @car
        if @car.update(car_params)
            flash[:notice] = "Le vehicule #{@car.name} a été modifié avec succès!"
            puts "hello2"
            redirect_to cars_app_index_path
        else
            puts "hello3"
            render :edit
        end
    end
  
     def destroy
        @car.user_id = current_user.id
        authorize @car
        if Rental.where(:car_id => @car.id).present?
          flash[:alert]="La voiture est en cours de location!"
          redirect_to cars_app_index_path
        else
          @car.destroy
          respond_to do |format|
              format.html { redirect_to cars_app_index_path, notice: 'Le vehicule a été supprimé avec succès.' }
              format.json { head :no_content }
          end
        end
    end
  
   private
  
      def set_car
        @car = Car.find(params[:id])
      end
  
      def car_params
        params.require(:car).permit(:name,:city , :description, :address, :plaque, :price_cents, :gearbox, :consommation, :engine, :nb_door, :places, :type_car, :price_by_km, :year,:statut, car_images: [])
      end
  end
  