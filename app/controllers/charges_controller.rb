class ChargesController < ApplicationController
    before_action :set_rental, only: [:new]
    before_action :set_car, only: [:new]
    before_action :acces_rental, only: [:new]

    def new
        puts ENV['PUBLISHABLE_KEY']

        if @acces_rental == false
          flash[:alert] = "Cette location ne vous appartient pas!"
          redirect_to rentals_path and return
        end
    end

    def create
      # Amount in cents
      
      rental = session[:rental]
      @rental = Rental.find(rental["id"])
      
      @amount = @rental.total_price_cents
      token = params[:stripeToken]
      @car = Car.find(@rental.car_id)
      puts ("car_id")
      puts @car
      Stripe.api_key = "sk_test_51IYXUAGO35ysxKum07Yn0Qut72dd8z2WvtzUZK3Q4LSGp6t1PVMgkUo7w4nlvmuiQJGBRAOC2zlyvLkXSdfk36TI00gnznX2J6"
      customer = Stripe::Customer.create({
        email: current_user.email,
        source: params[:stripeToken],
      })
      charge = Stripe::Charge.create({
        customer: customer.id,
        amount: @amount *100,
        description: "Location n°  #{@rental.id} / ClientID n°  #{current_user.id} / LoueurID n° #{@car.user_id} ",
        currency: 'eur'
      })


      @rental.update_attribute(:payment_status,true)
      

    rescue Stripe::CardError => e
      flash[:error] = e.message
      redirect_to new_charge_path
    end


    private 
    def acces_rental
      if current_user.id == @rental.user_id
        @acces_rental = true
      else
        @acces_rental = false
      end
    end

    def set_rental 
      @rentals = Rental.all
      @rental = @rentals.find(params[:rental])
      session[:rental] =  @rental
    end

    def set_car
      @car = Car.find(@rental.car_id)
    end
  end
