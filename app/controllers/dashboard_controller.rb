require 'oauth2'
require 'base64'
require 'uri'
require 'net/http'
require 'openssl'


class DashboardController < ApplicationController
    before_action :authenticate_user!
    before_action :set_xee_data, only: [:create_auth_code, :create_token, :create_token_curl]



    def index
        authorize User
    end

    def mifleetcars
        authorize User
        @cars = Car.all
    end

    def update_car_device
        authorize User
        if params[:device_id] == ""
            flash[:alert] = "Le device id est vide"
            redirect_to request.referrer
        else
            @car = Car.find(params[:car])
            if @car.update_attribute(:device_id,params[:device_id]) && @car.update_attribute(:society,"MiFleetXee")
                flash[:notice] = "Le device id du vehicule #{@car.name} a été modifié avec succès!"

                redirect_to mifleetcars_path
            else
                flash[:alert] = "Le device id du vehicule #{@car.name} n'a pas été modifé avec succès!"
                redirect_to request.referrer
            end
        end


    end


    def create_auth_code
        authorize User
        puts "I'm starting the GetXeeTokkenAccessJob job"


        session[:carA]= params[:carA]
        session[:carB]= params[:carB]
        session[:carC]= params[:carC]
        session[:account] = params[:account]
        puts "J'ai peter"
        if session[:account] == "compte1"
            puts ("compte1 aouth")
            @client = OAuth2::Client.new('82691cc6ca56aef8108137b13c6ce19d', 'a625efd93280973580ccf96acfc81b4d0cfc19a3379cde9e425f3ae5d113b2a9', site: 'https://api.xee.com', token_url: '/v4/oauth/token', authorize_url: '/v4/oauth/authorize')
            u = @client.auth_code.authorize_url(scope: 'vehicles.read vehicles.management vehicles.signals.read vehicles.locations.read vehicles.accelerometers.read vehicles.privacies.read vehicles.privacies.management vehicles.trips.read vehicles.loans.read vehicles.loans.management account.read vehicles.signals.summaries.read', redirect_uri: 'http://127.0.0.1:3005/login_token')
        elsif session[:account] == "compte2"
            puts ("compte2 aouth")
            @client = OAuth2::Client.new('aa06c7da19524436b5d40859b9e4f701', '0e4dc2f93910fd82270c9034207cabc1e1aea8c892adf1b4ccb7094173d7eeb3', site: 'https://api.xee.com', token_url: '/v4/oauth/token', authorize_url: '/v4/oauth/authorize')
            u = @client.auth_code.authorize_url(scope: 'vehicles.read vehicles.management vehicles.signals.read vehicles.locations.read vehicles.accelerometers.read vehicles.privacies.read vehicles.privacies.management vehicles.trips.read vehicles.loans.read vehicles.loans.management account.read vehicles.signals.summaries.read', redirect_uri: 'http://127.0.0.1:3005/login_token')
        end
        puts u

        redirect_to u

    end

    def create_token_curl
        code = params[:code]
        url = URI("https://api.xee.com/v4/oauth/token")

        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE

        request = Net::HTTP::Post.new(url)
        request.add_field("Authorization", "Basic ODI2OTFjYzZjYTU2YWVmODEwODEzN2IxM2M2Y2UxOWQ6YTYyNWVmZDkzMjgwOTczNTgwY2NmOTZhY2ZjODFiNGQwY2ZjMTlhMzM3OWNkZTllNDI1ZjNhZTVkMTEzYjJhOQ==")
        request["content-type"] = 'application/x-www-form-urlencoded'   
        puts ("hello 1")
        request.body = "grant_type=authorization_code&code=#{code}"
        puts ("hello 2")
        response = http.request(request)
        puts ("hello 3")
        puts response.read_body
        puts ("hello 4")

        redirect_to dashboard_index_path

    end
    def create_token

        authorize User

        code = params[:code]

        #puts "J'ai peter 2"
        # # => "https://example.org/oauth/authorization?response_type=code&client_id=client_id&redirect_uri=http://localhost:8080/oauth2/callback"

        if session[:account] == "compte1"
            puts ("compte1 entrer")
            @client = OAuth2::Client.new('82691cc6ca56aef8108137b13c6ce19d', 'a625efd93280973580ccf96acfc81b4d0cfc19a3379cde9e425f3ae5d113b2a9', site: 'https://api.xee.com', token_url: '/v4/oauth/token', authorize_url: '/v4/oauth/authorize')
            response = @client.auth_code.get_token( code , grant_type: "authorization_code",  headers: {'Authorization' => "Basic ODI2OTFjYzZjYTU2YWVmODEwODEzN2IxM2M2Y2UxOWQ6YTYyNWVmZDkzMjgwOTczNTgwY2NmOTZhY2ZjODFiNGQwY2ZjMTlhMzM3OWNkZTllNDI1ZjNhZTVkMTEzYjJhOQ==" })
        elsif  session[:account] == "compte2"
            puts ("compte2 entrer")
            @client = OAuth2::Client.new('aa06c7da19524436b5d40859b9e4f701', '0e4dc2f93910fd82270c9034207cabc1e1aea8c892adf1b4ccb7094173d7eeb3', site: 'https://api.xee.com', token_url: '/v4/oauth/token', authorize_url: '/v4/oauth/authorize')
            response = @client.auth_code.get_token( code , grant_type: "authorization_code",  headers: {'Authorization' => "Basic YWEwNmM3ZGExOTUyNDQzNmI1ZDQwODU5YjllNGY3MDE6MGU0ZGMyZjkzOTEwZmQ4MjI3MGM5MDM0MjA3Y2FiYzFlMWFlYThjODkyYWRmMWI0Y2NiNzA5NDE3M2Q3ZWViMw==" })
        else 
            flash[:alert] = "Token fail"
            redirect_to request.referrer
        end
        
            #puts "g p t 3"
        # response = token.get('/api/resource', params: {'query_foo' => 'bar'})
        # puts response
        # # => OAuth2::Response
        token = response.to_hash[:access_token]

        puts ("token")
        puts token

        if Car.find(session[:carA]).update_attribute(:XeeAccessTokken,token) && Car.find(session[:carB]).update_attribute(:XeeAccessTokken,token) && Car.find(session[:carC]).update_attribute(:XeeAccessTokken,token)
            flash[:notice] = "Token success"

            redirect_to mifleetcars_path
        else
            flash[:alert] = "Token fail"
            redirect_to request.referrer
        end


        # puts "OK I'm done now"
        # puts ("params code")
        # puts params["code"]
        # creer token
        # enregister token dans db
    end

    
    def customer
        @users = User.order(created_at: :asc).page(params[:page])
    end

    def getcustomer
        @user = User.find(params[:id])
    end

    def show_customer
        @user = User.find(params[:id])
        @verifications = Verification.where(:user_id => @user.id)
        @cars = Car.where(:user_id => @user.id)
        @rentals = Rental.where(:user_id => @user.id)
    end

    def vehicle
        @cars = Car.order(created_at: :asc).page(params[:page])
    end

    def orders
        @rentals = Rental.order(created_at: :asc).page(params[:page])
    end

    def search_customer
        query = params[:search_customer]
        @users = User.where('email LIKE ?', "%#{query}%")
  
    end

    def search_vehicle
        query = params[:search_vehicle]
        @cars = Car.where('name LIKE ?', "%#{query}%")

    end

    def search_orders
        query = params[:search_orders]
        @rentals = Rental.where('id LIKE ?', "%#{query}%")
        #render layout: false
    end

    def statistics
        @rentals = Rental.all
        @cars = Car.all
        @data = User.group_by_week(:created_at).count
        accumulator = 0
        @data.transform_values! do |val|
            val += accumulator
            accumulator = val
        end

    end

    def traccar_lock 
        @open_door_traccars = OpenDoorTraccar.all
    end
    

    private

    def set_xee_data
        puts ("data xee before action")
        @client_id     = '82691cc6ca56aef8108137b13c6ce19d' # your client’s id generated with rake db:setup
        @client_secret = 'a625efd93280973580ccf96acfc81b4d0cfc19a3379cde9e425f3ae5d113b2a9'
        @auth = 'Basic ' + Base64.encode64("#{@client_id}:#{@client_secret}") #issue with line jump instead same line
        
    end


end
