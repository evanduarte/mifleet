class EditCustomsController < Devise::RegistrationsController
    

    def edit

    end

    def update
        # unless @user.valid_password?(params[:user][:current_password])
        #     flash[:alert] = "Mot de passe incorrect!"
        #     redirect_to edit_user_registration_path
        # else
        #     flash[:notice] = "Modification effectué!"
        #     @user.update(params.require(:user).permit(:avatar))
        #     puts ("custom edit")
        #     redirect_to root_path
        # end
        self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
        prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

        resource_updated = update_resource(resource, account_update_params)
        yield resource if block_given?
        if resource_updated
        set_flash_message_for_update(resource, prev_unconfirmed_email)
        bypass_sign_in resource, scope: resource_name if sign_in_after_change_password?

        respond_with resource, location: after_update_path_for(resource)
        else
        clean_up_passwords resource
        set_minimum_password_length
        respond_with resource
        end
    end

    def destroy
        send(:"authenticate_#{resource_name}!", :force => true)
        self.resource = send(:"current_#{resource_name}")
        if Rental.where(:user_id => current_user.id).present?
            flash[:alert]="Vous avez des locations en cours!"
            redirect_to edit_user_registration_path
        else
            cars = Car.where(:user_id => current_user.id)
            cars.destroy_all
            resource.destroy
            Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
            set_flash_message :notice, :destroyed if is_navigational_format?
            respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
        end
    end


  
end
  
