class MainController < ApplicationController


    before_action :force_json, only: :search_json
  
    def index
    
    end
  
    

    def search_json
      @result = []
      @result_image = []
      @result_images = []
      @car_user = []
      @car_user_avatar = []
      @distance_between = []

      start_date = params[:search_start_date]
      end_date = params[:search_end_date]

      start_time = params[:search_start_time]
      end_time = params[:search_end_time]

      @distance = params[:search_distance].to_i

      #<input id= "date-ending", name="search_end_date" > => syntaxe doit etre exactement comme ca!!

      if @distance == 0
        flash[:alert] = "Veuillez renseigner la distance!"
        redirect_to root_path
      elsif params[:search_address] == ""
        flash[:alert] = "Veuillez renseigner une adresse de départ!"
        redirect_to root_path
      elsif start_date == "" && end_date==""
        flash[:alert] = "Veuillez renseigner une date!"
        redirect_to root_path
      elsif start_time == "" && end_time==""
        flash[:alert] = "Veuillez renseigner une heure!"
        redirect_to root_path
      else

        start_date_time = DateTime.parse([ start_date, start_time ].join(' '))
        end_date_time = DateTime.parse([ end_date,end_time ].join(' '))


        result_lat_lont = (Geocoder.search(params[:search_address])).first.coordinates
        @date_count = (end_date_time.to_date - start_date_time.to_date).to_i
        puts ("count date")
        puts @date_count
        if @date_count == 0
          @date_count = 1
        end
        @available_car_id = Rental.where("starts_on >= ? AND ends_on <= ?", start_date_time, end_date_time).pluck(:car_id)
        cars_available = Car.where.not(id: @available_car_id).near(result_lat_lont,5, units: :km)

        @available_cars = cars_available

        @available_cars.each_with_index do |car,index|
          car.first_image =  url_for(car.cars_thumbnail.first)
          @result << car
          @result_image << url_for(car.cars_thumbnail.first)
          @result_images[index] =[]
          @car_user << User.find(car.user_id)
          car.cars_thumbnail_show.each do |image| 
            @result_images[index] << url_for(image)
          end
         
        end

        @car_user.each do |user|
          @car_user_avatar << url_for(user.avatar_thumbnail)
        end
       

       
        #@result = @result.sort_by(&:distance)

        # ------------------------ REACT JS ---------------------------------

        @address = params[:search_address]

        gon.start_date = params[:search_start_date]
        gon.end_date = params[:search_end_date]

        @start_time = params[:search_start_time]
        @end_time = params[:search_end_time]

        gon.date_count = @date_count
        gon.distance = @distance

        #@results = Car.ransack(params[:q]).result(distinct: true)

        gon.results = @result
        gon.results_image = @result_image

        @cars = @result
        
        @rentals = Rental.all
        #@distance already up mentioned
      end
    end
  
    private
  
    def force_json
      request.format = :json
    end
  end