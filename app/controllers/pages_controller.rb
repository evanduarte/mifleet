class PagesController < ApplicationController

    def index

    end


    def home
      @cars = Car.all
      @rentals = Rental.all
      gon.invalidDates=""

      gon.date_start =""
      gon.date_end = ""

      @result_image = []
      gon.start_date = Date.today
      gon.end_date = Date.today

      @cars.each do |car|
        @result_image << url_for(car.cars_thumbnail.first)

      end



    end

    def search

      @result = []
      @result_image = []
      @result_images = []
      @latitude = []
      @longitude = []
      start_date = params[:search_start_date]
      end_date = params[:search_end_date]

      start_time = params[:search_start_time]
      end_time = params[:search_end_time]

      @distance = params[:search_distance].to_i

      #<input id= "date-ending", name="search_end_date" > => syntaxe doit etre exactement comme ca!!

      if @distance == 0
        flash[:alert] = "Veuillez renseigner la distance!"
        redirect_to root_path
      elsif params[:search_address] == ""
        flash[:alert] = "Veuillez renseigner une adresse de départ!"
        redirect_to root_path
      elsif start_date == "" && end_date==""
        flash[:alert] = "Veuillez renseigner une date!"
        redirect_to root_path
      elsif start_time == "" && end_time==""
        flash[:alert] = "Veuillez renseigner une heure!"
        redirect_to root_path
      else

        start_date_time = DateTime.parse([ start_date, start_time ].join(' '))
        end_date_time = DateTime.parse([ end_date,end_time ].join(' '))



        result_lat_lont = (Geocoder.search(params[:search_address])).first.coordinates
        @date_count = (end_date_time.to_date - start_date_time.to_date).to_i
        puts ("count date")
        puts @date_count
        if @date_count == 0
          @date_count = 1
        end
        @not_available_car_id = Rental.where("(starts_on <= :start_date_time AND ends_on >= :end_date_time) OR (starts_on >= :start_date_time AND ends_on <= :end_date_time)  OR (starts_on <= :start_date_time AND ends_on >= :start_date_time)  OR (starts_on <= :end_date_time AND ends_on >= :end_date_time)",{start_date_time: start_date_time, end_date_time: end_date_time}).pluck(:car_id)
        cars_available = Car.where.not(id: @not_available_car_id).near(result_lat_lont,5, units: :km)


        @available_cars = cars_available

        @available_cars.each_with_index do |car,index|
          images = []
          @result << car
          @result_image << url_for(car.cars_thumbnail.first)
          car.car_images.each do |image|
            images  << url_for(image)
          end
          @result_images << images
          #@result_images << url_for(car.car_images)
          @latitude << car.latitude
          @longitude << car.longitude
        end




        #@result = @result.sort_by(&:distance)

        # ------------------------ Result de la searchbar ---------------------------------

        @address = params[:search_address]

        @start_date = params[:search_start_date]
        @end_date = params[:search_end_date]


        gon.start_date = @start_date
        gon.end_date = @end_date

        @start_time = params[:search_start_time]
        @end_time = params[:search_end_time]

        gon.date_count = @date_count
        gon.distance = @distance

        gon.start_time_search=@start_time
        gon.end_time_search=@end_time

        #@results = Car.ransack(params[:q]).result(distinct: true)

        gon.results = @result
        gon.results_image = @result_image
        gon.results_images = @result_images
        gon.latitude=@latitude
        gon.longitude=@longitude

        gon.google_api_key = 'AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc'

        session[:start_date]=params[:search_start_date]
        session[:end_date]=params[:search_end_date]
        session[:start_time]=params[:search_start_time]
        session[:end_time]=params[:search_end_time]
        session[:distance]=@distance

        if user_signed_in?
          gon.current_user_id = current_user.id
        else
          gon.current_user_id = nil
        end


      end
    end


end





