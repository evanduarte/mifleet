class RentalsController < ApplicationController

  before_action :authenticate_user!, only: [:index, :show, :show_as_renter, :new, :new_search_car, :create, :edit, :update, :destroy]
  before_action :set_rental, only: [:show, :show_as_renter, :edit, :update, :destroy]
  before_action :acces_rental, only: [:show]
  before_action :acces_renter_rental, only: [:show_as_renter]

  def index
    #locataire
    @cars_locataire = []
    @users_locataire = []
    @car_image = []
    @rentals_locataire = Rental.all.where(user_id: current_user.id).order("id")
    @rentals_locataire.each do |rental|
      @cars_locataire << Car.find(rental.car_id)
      @users_locataire << User.find(Car.find(rental.car_id).user_id)
    end

    @cars_locataire.each do |car|
      @car_image << url_for(car.cars_thumbnail.first)
    end


    #proprio
    @users_proprio = []
    @user_avatar = []
    @cars_proprio = []
    @rentals_proprio = Rental.all.where(car_id: current_user.cars).order("id")
    @rentals_proprio.each do |rental|
      @cars_proprio << Car.find(rental.car_id)
      @users_proprio << User.find(rental.user_id)
    end

    @users_proprio.each do |user|
      @user_avatar << url_for(user.avatar_thumbnail)
    end

    #gon locataire
    gon.rentals_locataire = @rentals_locataire
    gon.car_image = @car_image
    gon.cars_locataire = @cars_locataire
    gon.users_locataire = @users_locataire

    #gon proprio
    gon.rentals_proprio = @rentals_proprio
    gon.user_avatar = @user_avatar
    gon.cars_proprio = @cars_proprio
    gon.users_proprio = @users_proprio

    gon.controller_name = controller_name
  
  end


  def show
    if @acces_rental == true
      @car = Car.find(@rental.car_id)
      @user = User.find(@car.user_id)
      @room_message = RoomMessage.new rental: @rental
      @room_messages = @rental.room_messages.includes(:user)
    else
      flash[:alert] = "Cette location ne vous appartient pas!"
      redirect_to rentals_path and return
    end
  end

  def show_as_renter
    @car = Car.find(@rental.car_id)
    @user = User.find(@rental.user_id)
    @room_message = RoomMessage.new rental: @rental
    @room_messages = @rental.room_messages.includes(:user)
   
  end

  def new
    @rental = Rental.new
    @cars = Car.all
    @car_info = @cars.find(params[:car])
    session[:car_attributes] = @car_info.id 

    @invalidDates = Rental.all.where(car_id: @car_info.id)
    gon.invalidDates=@invalidDates
    session[:from]=params[:from]
    if params[:from]== "show_car"
      gon.date_start = Date.today
      gon.date_end = Date.today + 1
      @time_start = ['7:30', '7:30:00']
      @time_end = ['10:00', '10:00:00']
      @distance_rental = ['Distance',0]
    elsif  params[:from]== "search_car"
      gon.date_start = session[:start_date]
      gon.date_end = session[:end_date]
      @time_start =  [session[:start_time], session[:start_time]]
      @time_end = [ session[:end_time], session[:end_time]]
      @distance_rental = ["#{session[:distance]} km",session[:distance]]

    end

  end

  def edit
    authorize @rental
    @rental = Rental.find(params[:id])

    if DateTime.now >= @rental.starts_on 
      flash[:alert]="Il est trop tard pour modifier votre commande!"
      redirect_to rentals_path
    else
      
      @cars = Car.all
      @car_info = @cars.find(@rental.car_id)
      @invalidDates = Rental.all.where(car_id: @car_info.id).where.not({ car_id: @rental.car_id, user_id: @rental.user_id, id: @rental.id })
     
      gon.invalidDates=@invalidDates
      gon.date_start =@rental.starts_on
      gon.date_end = @rental.ends_on

    end
    

  end


  def create
    @rental = Rental.new(rental_params)

    @cars = Car.all
    @car = @cars.find(params[:car_attributes])

    @invalidDates = Rental.all.where(car_id: params[:car_attributes])

    start_date = params[:rent_start_date]
    end_date = params[:rent_end_date]

    start_time = params[:rent_start_time]
    end_time = params[:rent_end_time]
    



    if @rental.distance == 0 && session[:from]=="show_car"
      flash[:alert] = "Veuillez renseigner la distance!"
      redirect_to new_rental_path(car: @car,from: "show_car")
    elsif start_date == "" && end_date=="" && session[:from]=="show_car"
      flash[:alert] = "Veuillez renseigner une date!"
      redirect_to new_rental_path(car: @car,from: "show_car")
    elsif @rental.distance == 0 &&  session[:from]=="search_car"
        flash[:alert] = "Veuillez renseigner la distance!"
        redirect_to new_rental_path(car: @car,from: "search_car")
    elsif start_date == "" && end_date=="" && session[:from]=="search_car"
      flash[:alert] = "Veuillez renseigner une date!"
      redirect_to new_rental_path(car: @car,from: "search_car")
    else
      

      @rental[:user_id] = current_user.id
      @rental[:car_id] = @car.id


      start_date_time = DateTime.parse([ start_date, start_time ].join(' '))
      end_date_time = DateTime.parse([ end_date,end_time ].join(' '))

      @rental[:starts_on] = start_date_time
      @rental[:ends_on] = end_date_time
      date_count = (end_date_time.to_date - start_date_time.to_date).to_i
      if date_count == 0
        date_count = 1
      end
      @rental[:total_price_cents] = (@car.price_cents * date_count) + (@rental.distance * @car.price_by_km)

      @invalidDates.each do |invalidDate| 
        d1 = DateTime.parse((invalidDate.starts_on).utc.strftime('%Y-%m-%d %H:%M:%p'))
        d2 = DateTime.parse((invalidDate.ends_on).utc.strftime('%Y-%m-%d %H:%M:%p'))


        if start_date_time.between?(d1,d2)==true && session[:from]=="show_car"
          flash[:alert] = "Date déja prise!"
          redirect_to new_rental_path(car: @car,from: "show_car") and return
        elsif end_date_time.between?(d1,d2)==true && session[:from]=="show_car"
          flash[:alert] = "Date déja prise!"
          redirect_to new_rental_path(car: @car,from: "show_car") and return
        elsif start_date_time.between?(d1,d2)==true && session[:from]=="search_car"
          flash[:alert] = "Date déja prise!"
          redirect_to new_rental_path(car: @car,from: "search_car") and return
        elsif end_date_time.between?(d1,d2)==true && session[:from]=="search_car"
          flash[:alert] = "Date déja prise!"
          redirect_to new_rental_path(car: @car,from: "search_car") and return
        end
      end



      # respond_to do |format|
      #   if @rental.save 
      #     @rental.update(room: "location n° #{@rental.id}")
      #     format.html { redirect_to new_charge_path(rental: @rental) and return }
      #     #format.html { redirect_to rentals_path }
      #     format.json { render :index, status: :created, location: @rental }
      #   else
      #     @car_info = @car
      #     @invalidDates = Rental.all.where(car_id: @car_info.id)
      #     gon.invalidDates=@invalidDates
      #     format.html { render :new }
      #     format.json { render json: @rental.errors, status: :unprocessable_entity }
      #   end
      # end

      respond_to do |format|
        if @rental.save 
          @rental.update(room: "location n° #{@rental.id}")
          format.html { redirect_to new_charge_path(rental: @rental) and return }
          #format.html { redirect_to rentals_path }
          format.json { render :index, status: :created, location: @rental }
        else
          @car_info = @car
          @invalidDates = Rental.all.where(car_id: @car_info.id)
          gon.invalidDates=@invalidDates
          format.html { render :new }
          format.json { render json: @rental.errors, status: :unprocessable_entity }
        end
      end
    end

  end

  def update
    @cars = Car.all
    @car = @cars.find(@rental.car_id)

    start_date = params[:rent_start_date]
    end_date = params[:rent_end_date]

    start_time = params[:rent_end_date]
    end_time = params[:rent_end_time]

    @rental.update(rental_params)
    if @rental.distance == 0
      flash[:alert] = "Veuillez renseigner la distance!"
      redirect_to edit_rental_path(@rental)
    elsif start_date == "" && end_date==""
      flash[:alert] = "Veuillez renseigner une date!"
      redirect_to edit_rental_path(car: @car)
    else
      @rental[:car_id] = @car.id
      start_date_time = DateTime.parse([ start_date, start_time ].join(' '))
      end_date_time = DateTime.parse([ end_date,end_time ].join(' '))

      @rental[:starts_on] = start_date_time
      @rental[:ends_on] = end_date_time
      date_count = (end_date_time.to_date - start_date_time.to_date).to_i
      if date_count == 0
        date_count = 1
      end
      @rental[:total_price_cents] = (@car.price_cents * date_count) + (@rental.distance * @car.price_by_km)
      @rental.update(rental_params)
      redirect_to rentals_path
    end
  end

  def destroy
    authorize @rental
    @rental = Rental.find(params[:id])
    if DateTime.now >= @rental.starts_on 
      flash[:alert]="Il est trop tard pour annuler votre commande!"
      redirect_to rentals_path
    else
      @rental.destroy
      flash[:notice]="Commande annulée!"
      redirect_to rentals_path
    end
    
  end


  private
    def set_rental
      @rental = Rental.find(params[:id])
    end

    def set_car
      @car = Car.find(params[:car_id])
    end

    def rental_params
      params.require(:rental).permit(:distance, :room)
    end

    def acces_rental
      if current_user.id == @rental.user_id
        @acces_rental = true
      else
        @acces_rental = false
      end
    end

    def acces_renter_rental
      @car = Car.find(@rental.car_id)
      if @car.user_id == @rental.user_id
        @acces_renter_rental = true
      else
        @acces_renter_rental = false
      end
    end

  

end
