class RentalsStepController < ApplicationController

    before_action :authenticate_user!, only: [:stepA, :stepB, :stepC, :stepD, :stepE, :update]
    before_action :set_rental, only: [:stepA, :stepB, :stepC, :stepD, :stepE, :update, :create_open_traccar]

    def stepA

        @car = Car.find(@rental.car_id)

        @hash = Gmaps4rails.build_markers(@car) do |car, marker|
            marker.lat car.latitude
            marker.lng car.longitude
        end
        
    
    end
  
    def stepB
        @car_image_faces = ["front_left", "front", "front_right", "left", "right", "rear_left", "rear", "rear_right"]
       
    end

    def stepC
        
    end

    def stepD
       
    end

    def stepE
        @car = Car.find(@rental.car_id)
    end

    def update
        authorize @rental
        if params[:step] == "E"
            @rental.update_attribute(:status,"in process")
            redirect_to @rental
        elsif @rental.update(rental_params)
            if params[:step] == "B"
                redirect_to stepC_path
            elsif params[:step] == "D"
                redirect_to stepE_path
            end
        else 
            flash[:alert] = "Veuillez remplir tous les champs!"
            redirect_to request.referrer
        end
    end

    def create_open_traccar
        @open_door_traccar = OpenDoorTraccar.new()
        @open_door_traccar[:car_id] = @rental.car_id
        @open_door_traccar[:user_id] = @rental.user_id
        @open_door_traccar[:rental_id] = @rental.id
        @open_door_traccar[:door_open] = true
        @open_door_traccar[:immobilizer] = true
        @open_door_traccar.save
        redirect_to request.referrer
    end



    private

    def set_rental
        @rental = Rental.find(params[:id])
    end

    def rental_params
        params.require(:rental).permit(:front_left, :front, :front_right, :left, :right, :rear_left, :rear, :rear_right, :status, img_car_inside: [])
    end

end
  