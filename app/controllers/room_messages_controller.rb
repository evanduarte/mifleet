class RoomMessagesController < ApplicationController
    before_action :load_entities

    def create
        if @rental.user_id == current_user.id
            @room_message = RoomMessage.create user: current_user,
                                        rental: @rental,
                                        message: params.dig(:room_message, :message),
                                        color: "#65C5FF"
        else 
            @room_message = RoomMessage.create user: current_user,
                                        rental: @rental,
                                        message: params.dig(:room_message, :message),
                                        color: "#16E1AF"
        end


        RoomChannel.broadcast_to @rental, @room_message

        redirect_to request.referrer
        
    end

    
    def load_entities
        @rental = Rental.find params.dig(:room_message, :rental_id)
    end
end
