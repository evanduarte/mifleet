require 'oauth2'
require 'base64'
require 'uri'
require 'net/http'
require 'openssl'
require 'rest-client'


class TraccarController < ApplicationController
    rescue_from ActiveRecord::RecordNotUnique, with: :invalid_foreign_key
    before_action :authenticate_user!
    before_action :set_car, only: [:edit]
    before_action :set_traccar_car, only: [:traccar_edit, :search_traccar_connected]
    before_action :traccar_commands, only: [:traccar_edit, :traccar_command]


    def index
        @cars = Car.order(created_at: :asc).page(params[:page])

        $url_device = "86.247.136.45:8082/api/devices"
        response = RestClient::Request.execute(url: $url_device, method: :get, headers:{Authorization: "Basic #{Base64.encode64('huangdey973@gmail.com:Parisevan1995')}"})
        
        @data = JSON.parse(response)

        @uniqueId = []

        @data.each do |device|
            @uniqueId << device["uniqueId"]
    
        end

  

        render layout: "dashboard"
    end

    def search_traccar
        query = params[:search_traccar]
        @cars = Car.where('name LIKE ?', "%#{query}%")
        #render layout: false
    end

    def search_traccar_connected
        query = params[:search_traccar_connedted]
        @data = @data.where('name LIKE ?', "%#{query}%")
        #render layout: false
    end

    def edit
        render layout: "dashboard"
    end

    def add_device_id
        authorize User
        @car = Car.find(params[:car_id])
        if @car.update_attribute(:device_id,params[:device_id])
            flash[:notice] = "Le device id du vehicule #{@car.name} a été modifié avec succès!"
            redirect_to traccar_index_path
        else
            flash[:alert] = "Le device id du vehicule #{@car.name} n'a pas été modifé avec succès!"
            redirect_to request.referrer
        end
     
    end

    def traccar_edit

        render layout: "dashboard"
    end

    def traccar_command
   
        response =  RestClient::Request.execute(method: :post, url: '86.247.136.45:8082/api/commands/send', payload: {id: params[:id], deviceId: params[:traccar_id] }.to_json, headers:{Authorization: "Basic #{Base64.encode64('huangdey973@gmail.com:Parisevan1995')}", :accept => "application/json", :content_type => "application/json" })

        @commands.each do |command|  
            if params[:id].to_i == command["id"]
                @command = command
            end
        end
        #render json: response
        flash[:notice] = "La commande < #{@command["description"]} > a été éxécutée avec succès!"
        
        redirect_to request.referrer

        
    end

    def new
        render layout: "dashboard"
    end

    def add_traccar_device

    end

    def traccar_api
    



        $url = "86.247.136.45:8082/api/devices"


        response = RestClient::Request.execute(url: $url, method: :get, headers:{Authorization: "Basic #{Base64.encode64('huangdey973@gmail.com:Parisevan1995')}"})
        #response =  RestClient::Request.execute(method: :post, url: '86.247.136.45:8082/api/commands/send', payload: {id: 2, deviceId: 864180036851835 }.to_json, headers:{Authorization: "Basic #{Base64.encode64('huangdey973@gmail.com:Parisevan1995')}", :accept => "application/json", :content_type => "application/json" })

        puts ("traccar")
        
        @data = JSON.parse(response)

        # @uniqueId = []

        # @data.each do |device|
        #     @uniqueId << device["uniqueId"]
    
        # end


        #render json: nil, status: :ok
        render json: response
        #head :ok


        #----------------------

        # response = RestClient::Request.new({
        #     method: :post,
        #     url:  $url,
        #     user: 'huangdey973@gmail.com',
        #     password: 'Parisevan1995',
        #     payload: { id: 1, deviceId: 864180036851835 , description:"", type:"", attributes: ""},
        #     headers: { :accept => "application/json", :content_type => "application/json" }
        #   }).execute do |response, request, result|
        #     case response.code
        #     when 202
        #       [ :error, parse_json(response.to_str) ]
        #     when 200
        #       [ :success, parse_json(response.to_str) ]
        #     else
        #       fail "Invalid response #{response.to_str} received."
        #     end
        #   end
          
        #   render json: response
      
        
    end

    private
    
    def set_car
        @car = Car.find(params[:id])
    end

    def set_traccar_car 
        $url_device = "86.247.136.45:8082/api/devices"
        response = RestClient::Request.execute(url: $url_device, method: :get, headers:{Authorization: "Basic #{Base64.encode64('huangdey973@gmail.com:Parisevan1995')}"})
        
        @data = JSON.parse(response)

        @data.each do |device|  
            if params[:id].to_i == device["id"]
                @traccar = device
            end
        end

    end

    def car_params
        params.require(:car).permit(:device_id)
    end

    def traccar_commands
        $url = "86.247.136.45:8082/api/commands"

        response = RestClient::Request.execute(url: $url, method: :get, headers:{Authorization: "Basic #{Base64.encode64('huangdey973@gmail.com:Parisevan1995')}"})
    
        
        @commands = JSON.parse(response)

    end

    def invalid_foreign_key
        redirect_to request.referrer, alert: 'Device id déja associé.'
    end
    
end