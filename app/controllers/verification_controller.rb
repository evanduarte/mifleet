

class VerificationController < ApplicationController
    #before_action :force_json, only: [:submit_verification]

    before_action :set_verification, only: [:edit, :update]


    def initialize
        @base_uri = 'https://firebasestorage.googleapis.com/v0/b/mifleetv3.appspot.com/o/'
        private_key_json_string = File.open('mifleetv3-firebase-adminsdk-rdhno-c77dc00054.json').read
        @firebase = Firebase::Client.new(@base_uri, private_key_json_string)
        @token = '6435d0ef-13e9-408f-8719-3a0a77fcaf8d'
    end

    def index
        @verification_driving_license = Verification.all.where(identity_type: "driving_license")
        @verification_cni = Verification.all.where(identity_type: "cni")
        @verification_cni = @verification_cni.order(created_at: :asc).page(params[:page])

        render layout: "dashboard"
    end

    def edit
        @imageFront = "#{@base_uri}#{@verification.imageFront}?alt=media&token=#{@token}"
        @imageBack = "#{@base_uri}#{@verification.imageBack}?alt=media&token=#{@token}"
        render layout: "dashboard"
    end

    def update

        if @verification.update_attribute(:status,params[:verification]["status"])
            if params[:verification]["status"] == "validated"
                flash[:notice] = "Vérification validée!"
                redirect_to verification_index_path
            elsif params[:verification]["status"] == "rejected"
                flash[:alert] = "Vérification rejetée!"
                redirect_to verification_index_path
            end
        end
    end


    # def submit_verification

    #     @verification = Verification.new()

    #     @verification[:identity_type] = params[:identity_type]
    #     @verification[:user_id] = params[:user_id]
    #     if params[:status] = "await"
    #         @verification[:status] = 0
    #     elsif params[:status] = "validated"
    #         @verification[:status] = 1
    #     elsif params[:status] = "rejected"
    #         @verification[:status] = 2
    #     end

    #     puts "verif"
    #     puts @verification
    #     if @verification.save
    #         @answer = "Votre demande est en cours de vérification."
    #     else
    #         @answer = "Votre demande n'est pas valide"
    #     end
        
    # end

    def create 
        @verification = Verification.new()

        @verification[:identity_type] = params[:identity_type]
        @verification[:user_id] = params[:user_id]
        if params[:status] = "await"
            @verification[:status] = 0
        elsif params[:status] = "validated"
            @verification[:status] = 1
        elsif params[:status] = "rejected"
            @verification[:status] = 2
        end

       
        @verification[:imageFront]=params[:imageFront]
        @verification[:imageBack]=params[:imageBack]

        
        if @verification.save
            @answer = "Votre demande est en cours de vérification."
        else
            @answer = "Votre demande n'est pas valide"
        end

        render :layout => false

    end
 
       

    private
  
    def force_json
        request.format = :json
    end

    def verification_params
        params.require(:verification).permit(:identity_type, :status, :user_id, :imageFront, :imageBack)
    end

    def set_verification
        @verification = Verification.find(params[:id])
    end
end