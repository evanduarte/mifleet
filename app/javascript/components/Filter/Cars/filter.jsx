import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';


const HiddenCheckbox = styled.input.attrs({ type: 'checkbox' })`
  // Hide checkbox visually but remain accessible to screen readers.
  // Source: https://polished.js.org/docs/#hidevisually
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
  `


  const StyledCheckbox = styled.div`
  display: inline-block;
  width: 20px;
  height: 20px;
  background: ${props => props.checked ? '#6EB590' : '#d6eae0'};
  border-radius: 3px;
  transition: all 150ms;
  ${Icon} {
    visibility: ${props => props.checked ? 'visible' : 'visible'}
  }
`

const CheckboxContainer = styled.div`
  display: inline-block;
  vertical-align: middle;
  margin-right:5px;
`

const Icon = styled.svg`
  fill: none;
  stroke: white;
  stroke-width: 2px;
`


const Checkbox = ({ className, checked, ...props }) => (
  <CheckboxContainer className={className}>
    <HiddenCheckbox checked={checked} {...props} />
    <StyledCheckbox checked={checked}>
      <Icon viewBox="0 0 24 24">
        <polyline points="20 6 9 17 4 12" />
      </Icon>
    </StyledCheckbox>
  </CheckboxContainer>
)


class Filter extends Component {
    constructor(props) {
      super(props);
      this.state = {
        disabled: false,
        type_car: [
          {id: 1, key:1, name: "Utilitaire", value: "Utilitaire",checked: false},
          {id: 2, key:2, name: "Citadine", value: "Citadine",checked: false},
          {id: 3, key:3, name: "Compacte", value: "Compacte",checked: false},
          {id: 4, key:4, name: "Minibus", value: "Minibus",checked: false}
        ],
        gearbox: [
          {id: 1, key:1, name: "Manuelle", value: "Manuelle",checked: false},
          {id: 2, key:2, name: "Automatique", value: "Automatique",checked: false}
        ],
        consommation: [
          {id: 1, key:1, name: "Diesel", value: "Diesel",checked: false},
          {id: 2, key:2, name: "Essences", value: "Essences",checked: false},
          {id: 3, key:3, name: "Hybride", value: "Hybride",checked: false},
          {id: 4, key:4, name: "Electrique", value: "Electrique",checked: false}
        ],
        places: 9,
        open: false,
        openFilter: false
      };

    }
    handleOnChange = (e) => {
      console.log("handleOnChange");
      console.log(e.target.value, e.target.name);
      this.props.handleSearchEvents(e.target.value, e.target.name);
    };



    handleOnChecked = (e) => {
      let type_cars = this.state.type_car
      let valArr = []
      type_cars.forEach(type_car => {
        if (type_car.value === e.target.value) {
          type_car.checked =  e.target.checked
          this.setState({ checked: e.target.checked })
          console.log('Checkbox checked:', (e.target.checked));
        }
        if (type_car.checked === true) {
          valArr.push(type_car.value);
          console.log("handleOnChecked");
          console.log(type_car.value, type_car.checked);
        }

      })
      console.log("valArr :",valArr);
      console.log("value :",e.target.value);
      this.props.handleSearchEvents(valArr, e.target.name);
    };

    handleOnCheckedGearbox = (e) => {
      let gearboxes = this.state.gearbox
      let valArr = []
      gearboxes.forEach(gearbox => {
        if (gearbox.value === e.target.value) {
          gearbox.checked =  e.target.checked
          this.setState({ checked: e.target.checked })
          console.log('gearbox checked:', (e.target.checked));
        }
        if (gearbox.checked === true) {
          valArr.push(gearbox.value);
          console.log("handleOnChecked");
          console.log(gearbox.value, gearbox.checked);
        }

      })
      console.log("valArr :",valArr);
      console.log("value :",e.target.value);
      this.props.handleSearchEvents(valArr, e.target.name);
    };

      handleOnCheckedConsommation = (e) => {
        let consommations = this.state.consommation
        let valArr = []
        consommations.forEach(consommation => {
          if (consommation.value === e.target.value) {
            consommation.checked =  e.target.checked
            this.setState({ checked: e.target.checked })
            console.log('consommation checked:', (e.target.checked));
          }
          if (consommation.checked === true) {
            valArr.push(consommation.value);
            console.log("handleOnChecked");
            console.log(consommation.value, consommation.checked);
          }

        })
      console.log("valArr :",valArr);
      console.log("value :",e.target.value);
      this.props.handleSearchEvents(valArr, e.target.name);
    };


    handleOnPlaces = range => {
      console.log(range)
      this.setState({places: range});
      this.props.onRangeChange(range,"places");
    };

    toggle = () => {
      this.setState({ open: !this.state.open });
      console.log("toggle in");
    };

    toggleFilter = () => {
      this.setState({ openFilter: !this.state.openFilter });
      console.log("filter in");
    };







    render() {
        return (
          <div className="filter-mobile">
            {/* <div>
              <input
                  type="text"
                  name='name'
                  value={this.props.name}
                  onChange={this.handleOnChange}
                  placeholder='Name...'/>
            </div> */}
            <div className="filter-button" onClick={this.toggleFilter}>
              <h4><strong>{'Filtrer'}</strong></h4>
            </div>
            <div className="filter-search">
              <div>
                <h5><strong>Type de véhicule</strong></h5>
                  {
                    this.state.type_car.map((type) => {
                      return (
                      <div>
                        <label>
                          <Checkbox
                            name='type_car'
                            type='number'
                            value = {type.value}
                            checked={type.checked}
                            onChange={this.handleOnChecked}
                          />
                          <span>{type.name}</span>
                        </label>
                      </div>

                      );

                    })
                  }
              </div>
              <div className="places">
                <h5><strong>Nombre de places max</strong></h5>
                <InputRange
                  name='places'
                  maxValue={9}
                  minValue={2}
                  value={this.state.places}
                  onChange={this.handleOnPlaces}/>
              </div>
              <div>
                <h5><strong>Boite de vitesse</strong></h5>
                {
                    this.state.gearbox.map((gear) => {
                      return (
                      <div>
                        <label>
                          <Checkbox
                            name='gearbox'
                            type='number'
                            value = {gear.value}
                            checked={gear.checked}
                            onChange={this.handleOnCheckedGearbox}
                          />
                          <span>{gear.name}</span>
                        </label>
                      </div>

                      );

                    })
                  }
              </div>

              <div>
                <h5><strong>Energie</strong></h5>
                  {
                    this.state.consommation.map((cons) => {
                      return (
                      <div>
                        <label>
                          <Checkbox
                            name='consommation'
                            type='number'
                            value = {cons.value}
                            checked={cons.checked}
                            onChange={this.handleOnCheckedConsommation}
                          />
                          <span>{cons.name}</span>
                        </label>
                      </div>

                      );

                    })
                  }
              </div>
            </div>

            <Drawer
              open={this.state.openFilter}
              onRequestClose={this.toggleFilter}
              onDrag={() => {}}
              onOpen={() => {}}
              allowClose={true}
              modalElementClass="HugeListFilter"
              containerElementClass="my-shade-mobile"
              parentElement={document.body} // element to be appended to
              direction="right"
            >
              <div className="drawer-filter">
                <div>
                  <h5><strong>Type de véhicule</strong></h5>
                    {
                      this.state.type_car.map((type) => {
                        return (
                        <div>
                          <label>
                            <Checkbox
                              name='type_car'
                              type='number'
                              value = {type.value}
                              checked={type.checked}
                              onChange={this.handleOnChecked}
                            />
                            <span>{type.name}</span>
                          </label>
                        </div>

                        );

                      })
                    }
                </div>
                <div className="places">
                  <h5><strong>Nombre de places max</strong></h5>
                  <InputRange
                    name='places'
                    maxValue={9}
                    minValue={2}
                    value={this.state.places}
                    onChange={this.handleOnPlaces}/>
                </div>
                <div>
                  <h5><strong>Boite de vitesse</strong></h5>
                  {
                      this.state.gearbox.map((gear) => {
                        return (
                        <div>
                          <label>
                            <Checkbox
                              name='gearbox'
                              type='number'
                              value = {gear.value}
                              checked={gear.checked}
                              onChange={this.handleOnCheckedGearbox}
                            />
                            <span>{gear.name}</span>
                          </label>
                        </div>

                        );

                      })
                    }
                </div>

                <div>
                  <h5><strong>Consommation</strong></h5>
                    {
                      this.state.consommation.map((cons) => {
                        return (
                        <div>
                          <label>
                            <Checkbox
                              name='consommation'
                              type='number'
                              value = {cons.value}
                              checked={cons.checked}
                              onChange={this.handleOnCheckedConsommation}
                            />
                            <span>{cons.name}</span>
                          </label>
                        </div>

                        );

                      })
                    }
                </div>
              </div>
            </Drawer>

          </div>




        )
    }
}


export default Filter;
