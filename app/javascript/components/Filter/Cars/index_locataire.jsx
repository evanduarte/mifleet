


import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';

import Filter from "./filter"
import Results from "./results"






class ResultsIndexCars extends Component {
    constructor(props) {
      super(props);
      this.data = [];
      this.state = {
        name: '',
        type_car: [],
        plaque: '',
        gearbox: [],
        consommation: [],
        places: 9,
        isLoading: true,
        open: false,
        activeItemIndex: 0,

      };
     }  
      
    componentWillMount() {       
        let array = []; 
        for (let i = 0; i < (gon.results_all_cars).length; i++) {             
            let entry = {}; 
            entry.id = (gon.results_all_cars)[i].id; 
            entry.user_id = (gon.results_all_cars)[i].user_id; 
            entry.plaque = (gon.results_all_cars)[i].plaque.toLowerCase(); 
            entry.plaqueMaj = (gon.results_all_cars)[i].plaque; 
            entry.name = (gon.results_all_cars)[i].name;     
            entry.year = (gon.results_all_cars)[i].year; 
            entry.engine = (gon.results_all_cars)[i].engine;    
            entry.places = (gon.results_all_cars)[i].places; 
            entry.gearbox = (gon.results_all_cars)[i].gearbox; 
            entry.consommation = (gon.results_all_cars)[i].consommation;  
            entry.price_cents = (gon.results_all_cars)[i].price_cents;          
            entry.price_by_km = (gon.results_all_cars)[i].price_by_km;    
            entry.type_car = (gon.results_all_cars)[i].type_car;                  
            entry.address = (gon.results_all_cars)[i].address;
            entry.description = (gon.results_all_cars)[i].description;
            entry.image = ((gon.results_image_all_cars)[i]);
            array[i] = entry;       
        } 
        this.data = array;       
        this.setState({          
            isLoading: false,
        }); 
    }

 
    



    handleSearchEvents = (title, name) => {
        this.setState({ [name]: title });
    }

    onRangeChange = (range,name) => {
      this.setState({
        [name]: {
          ...this.state[name],
          value: range
        }
      });
    };

    toggle = () => {
      this.setState({ open: !this.state.open });

    };



    changeActiveItem = (activeItemIndex) => this.setState({ activeItemIndex });
    

    render() {
        const filteredData = this.data.filter((dataObj)=>
          (dataObj.user_id !== gon.current_user_id)
          &&
          (dataObj.name.indexOf(this.state.name) !== -1)
          &&
          (    
            (dataObj.type_car.indexOf(this.state.type_car) !== -1)||
            (    
              (dataObj.type_car.indexOf(this.state.type_car[0]) !== -1)||
              (dataObj.type_car.indexOf(this.state.type_car[1]) !== -1)||
              (dataObj.type_car.indexOf(this.state.type_car[2]) !== -1)||
              (dataObj.type_car.indexOf(this.state.type_car[3]) !== -1)
            )          
          )
          &&
          (
            dataObj.places <= (this.state.places.value) ||
            dataObj.places <= (this.state.places)
          )
          &&
          (    
            (dataObj.gearbox.indexOf(this.state.gearbox) !== -1)||
            (    
              (dataObj.gearbox.indexOf(this.state.gearbox[0]) !== -1)||
              (dataObj.gearbox.indexOf(this.state.gearbox[1]) !== -1)
            )          
          )&&
          (    
            (dataObj.consommation.indexOf(this.state.consommation) !== -1)||
            (    
              (dataObj.consommation.indexOf(this.state.consommation[0]) !== -1)||
              (dataObj.consommation.indexOf(this.state.consommation[1]) !== -1)||
              (dataObj.consommation.indexOf(this.state.consommation[2]) !== -1)||
              (dataObj.consommation.indexOf(this.state.consommation[3]) !== -1)||
              (dataObj.consommation.indexOf(this.state.consommation[4]) !== -1)
            )          
          )
        );
       
        
        return (
            <div className="display-page">
              <div className="display-filter">
                <Filter 
                name={this.state.name}
                type_car={this.state.type_car}
                places={this.state.places}
                gearbox={this.state.gearbox}
                consommation={this.state.consommation}
                id={this.state.id}
                handleSearchEvents={this.handleSearchEvents}
                onRangeChange={this.onRangeChange}
                toggleFilter={this.toggleFilter}/>


              </div>

                <Results  
                name={this.state.name}
                type_car={this.state.type_car}
                places={this.state.places}
                gearbox={this.state.gearbox}
                consommation={this.state.consommation}
                id={this.state.id}
                activeItemIndex={this.state.activeItemIndex}
                changeActiveItem={this.changeActiveItem}
                data={this.data}
                data={filteredData}/>
            
            </div>
        )
    }
}







  document.addEventListener('turbolinks:load', () => {
    if (document.getElementById('cars_results') != null) {

      ReactDOM.render(
        <ResultsIndexCars />,
        document.getElementById('cars_results'),
      )
    }
  });

  // document.onunload= function() {
  //   if (document.getElementById('cars_results') != null) {
  //     console/log("unload");
  //     ReactDOM.render(
  //       <ResultsIndexCars />,
  //       document.getElementById('cars_results'),
  //     )
  //   }
  // };



  


