

console.log("enter in mycarsindex")
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';

import FilterPlaque from "./filterByPlaque"
import Results from "./results_proprio"






class ResultsIndexMyCars extends Component {
    constructor(props) {
      super(props);
      this.data = [];
      this.state = {
        name: '',
        type_car: [],
        plaque: '',
        gearbox: [],
        consommation: [],
        places: 9,
        isLoading: true,
        open: false,
        activeItemIndex: 0,

      };
     }  
      
    componentWillMount() {       
        let array = []; 
        for (let i = 0; i < (gon.results_all_cars).length; i++) {             
            let entry = {}; 
            entry.id = (gon.results_all_cars)[i].id; 
            entry.user_id = (gon.results_all_cars)[i].user_id; 
            entry.plaque = (gon.results_all_cars)[i].plaque.toLowerCase(); 
            entry.plaqueMaj = (gon.results_all_cars)[i].plaque; 
            entry.name = (gon.results_all_cars)[i].name;     
            entry.year = (gon.results_all_cars)[i].year; 
            entry.engine = (gon.results_all_cars)[i].engine;    
            entry.places = (gon.results_all_cars)[i].places; 
            entry.gearbox = (gon.results_all_cars)[i].gearbox; 
            entry.consommation = (gon.results_all_cars)[i].consommation;  
            entry.price_cents = (gon.results_all_cars)[i].price_cents;          
            entry.price_by_km = (gon.results_all_cars)[i].price_by_km;    
            entry.type_car = (gon.results_all_cars)[i].type_car;                  
            entry.address = (gon.results_all_cars)[i].address;
            entry.description = (gon.results_all_cars)[i].description;
            entry.image = ((gon.results_image_all_cars)[i]);
            array[i] = entry;       
        } 
        this.data = array;       
        this.setState({          
            isLoading: false,
        }); 
        
    }

 
    



    handleSearchEvents = (title, name) => {
        this.setState({ [name]: title });
    }


    render() {
        const filteredData = this.data.filter((dataObj)=>
          (dataObj.plaque.indexOf(this.state.plaque) !== -1) && (dataObj.user_id === gon.current_user_id)
        );
      
        
        return (
            <div className="display-page_my_cars">
              <h5 style={{textAlign:"center"}}>Mes voitures</h5> 
              <div className="display-filter_my_cars">
                <FilterPlaque 
                name={this.state.name}
                plaque={this.state.plaque}
                type_car={this.state.type_car}
                places={this.state.places}
                gearbox={this.state.gearbox}
                consommation={this.state.consommation}
                id={this.state.id}
                handleSearchEvents={this.handleSearchEvents}/>


              </div>

                <Results 
                name={this.state.name}
                plaque={this.state.plaque}
                type_car={this.state.type_car}
                places={this.state.places}
                gearbox={this.state.gearbox}
                consommation={this.state.consommation}
                id={this.state.id}
                activeItemIndex={this.state.activeItemIndex}
                data={this.data}
                data={filteredData}/>
            
            </div>
        )
    }
}







  document.addEventListener('turbolinks:load', () => {
    if (document.getElementById('my_cars_results') != null) {
        ReactDOM.render(
        <ResultsIndexMyCars />,
        document.getElementById('my_cars_results'),
        )
    }
  });



