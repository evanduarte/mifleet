

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';

import Filter from "./filter"
import Results from "./results_search"






class ResultsSearchCar extends Component {
    constructor(props) {
      super(props);
      this.data = [];
      this.state = {
        name: '',
        type_car: [],
        plaque: '',
        gearbox: [],
        consommation: [],
        places: 9,
        isLoading: true,
        open: false,
        activeItemIndex: 0,

      };
     }  
      
    componentWillMount() {       
        let array = []; 
        for (let i = 0; i < (gon.results).length; i++) {             
            let entry = {}; 
            entry.id = (gon.results)[i].id; 
            entry.user_id = (gon.results)[i].user_id; 
            entry.plaque = (gon.results)[i].plaque.toLowerCase(); 
            entry.plaqueMaj = (gon.results)[i].plaque; 
            entry.name = (gon.results)[i].name;     
            entry.year = (gon.results)[i].year; 
            entry.engine = (gon.results)[i].engine;    
            entry.places = (gon.results)[i].places; 
            entry.gearbox = (gon.results)[i].gearbox; 
            entry.consommation = (gon.results)[i].consommation;  
            entry.price_cents = (gon.results)[i].price_cents;          
            entry.price_by_km = (gon.results)[i].price_by_km;    
            entry.type_car = (gon.results)[i].type_car;                  
            entry.address = (gon.results)[i].address;
            entry.description = (gon.results)[i].description;
            entry.image = ((gon.results_image)[i]);
            entry.images = ((gon.results_images)[i]);
            entry.latitude = ((gon.latitude)[i]);
            entry.longitude = ((gon.longitude)[i]);
            entry.activeItemIndex=0;
            array[i] = entry;       
        } 
        this.data = array;       
        this.setState({          
            isLoading: false,
        });       
    }

    handleSearchEvents = (title, name) => {
        this.setState({ [name]: title });
    }

    onRangeChange = (range,name) => {
      this.setState({
        [name]: {
          ...this.state[name],
          value: range
        }
      });
    };

    toggle = () => {
      this.setState({ open: !this.state.open });
    };



    changeActiveItem = (activeItemIndex) => this.setState({ activeItemIndex });
    

    render() {
        const filteredData = this.data.filter((dataObj)=>
          (dataObj.user_id !== gon.current_user_id)
          &&
          (dataObj.name.indexOf(this.state.name) !== -1)
          &&
          (    
            (dataObj.type_car.indexOf(this.state.type_car) !== -1)||
            (    
              (dataObj.type_car.indexOf(this.state.type_car[0]) !== -1)||
              (dataObj.type_car.indexOf(this.state.type_car[1]) !== -1)||
              (dataObj.type_car.indexOf(this.state.type_car[2]) !== -1)||
              (dataObj.type_car.indexOf(this.state.type_car[3]) !== -1)
            )          
          )
          &&
          (
            dataObj.places <= (this.state.places.value) ||
            dataObj.places <= (this.state.places)
          )
          &&
          (    
            (dataObj.gearbox.indexOf(this.state.gearbox) !== -1)||
            (    
              (dataObj.gearbox.indexOf(this.state.gearbox[0]) !== -1)||
              (dataObj.gearbox.indexOf(this.state.gearbox[1]) !== -1)
            )          
          )&&
          (    
            (dataObj.consommation.indexOf(this.state.consommation) !== -1)||
            (    
              (dataObj.consommation.indexOf(this.state.consommation[0]) !== -1)||
              (dataObj.consommation.indexOf(this.state.consommation[1]) !== -1)||
              (dataObj.consommation.indexOf(this.state.consommation[2]) !== -1)||
              (dataObj.consommation.indexOf(this.state.consommation[3]) !== -1)||
              (dataObj.consommation.indexOf(this.state.consommation[4]) !== -1)
            )          
          )
        );
    

        return (
            <div className="display-page">
              <div className="display-filter">
                <Filter 
                name={this.state.name}
                type_car={this.state.type_car}
                places={this.state.places}
                gearbox={this.state.gearbox}
                consommation={this.state.consommation}
                id={this.state.id}
                handleSearchEvents={this.handleSearchEvents}
                onRangeChange={this.onRangeChange}
                toggleFilter={this.toggleFilter}/>


              </div>

                <Results  
                name={this.state.name}
                type_car={this.state.type_car}
                places={this.state.places}
                gearbox={this.state.gearbox}
                consommation={this.state.consommation}
                id={this.state.id}
                activeItemIndex={this.state.activeItemIndex}
                changeActiveItem={this.changeActiveItem}
                data={this.data}
                data={filteredData}/>
            
            </div>
        )
    }
}







  document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(
      <ResultsSearchCar />,
      document.getElementById('results'),
    )
  });



   

