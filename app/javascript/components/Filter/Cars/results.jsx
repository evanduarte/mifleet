import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';




class Results extends Component {

    constructor(props) {
      super(props);
      this.state = {
        activeItemIndex: 0
      }
    };





    toggle = () => {
        this.setState({ open: !this.state.open });
        console.log("toggle in");
    };

    changeActiveItem = (activeItemIndex) => this.setState({ activeItemIndex });

    render() {
      let rows = [];

      this.props.data.forEach((dataObj) => {
          rows.push(
                  <div className="card">

                    <img src={dataObj.image} className="image-size"/>
                    <div className="card-description">
                      <h2><span style={{fontSize: 50}}>{dataObj.price_cents}</span>€/Jrs</h2>
                      <h2>{dataObj.name} </h2>
                      <p>{dataObj.year} , Moteur {dataObj.engine} ,  {dataObj.places} Places, {dataObj.gearbox} </p>

                    </div>

                    <a href={document.URL + "/" + dataObj.id} className= 'card-link'></a>
                  </div>

            );



      });
      return (

          <div>
            <div className="display-cars">
              {rows}
            </div>
          </div>

      )
    }
}





export default Results;
