

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';



class Results extends Component {  
    
    constructor(props) {
      super(props);
      this.state = {
        plaque:'',
        activeItemIndex: 0
      }
    };
    




    render() {   
      let rows = []; 
    
      this.props.data.forEach((dataObj) => { 

        rows.push(

                <div className="my_cars_card card">
                  
                  <img src={dataObj.image} className="image-size-my-cars"/>
                  <div className="my_car_description">
                    <h2><span style={{fontSize: 16}}>{dataObj.name}</span></h2>
                    <h2 className="address_my_cars">{dataObj.address} </h2>
                    <p>{dataObj.plaqueMaj} </p>
                    
                  </div>
                  <a href={document.URL + "/" + dataObj.id} className= 'card-link'></a>
                </div>


                
          );
        
 
      });     
      return (      

            <div className="display-my_cars">
              {rows} 
            </div>
           

      ) 
    }
}
export default Results;