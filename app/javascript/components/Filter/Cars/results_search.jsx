

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';

let show = [];
for (let i = 0; i < (gon.results).length; i++) {
  show[(gon.results)[i].id]= false;
}

class Results extends Component {

    constructor(props) {
      super(props);
      this.state = {
        show,
        open: false,
        activeItemIndex: 0
      }
    };


    showComponent = car_id => {

      let show_car = {...this.state.show[car_id]};

      //show_car = this.state.show[car_id]? false : true;
      if (show[car_id] == false) {
        show_car = true;
        show[car_id]= show_car;
        this.setState({show});
      }
      console.log(show);

    };

    handleClick = (car_id) => {

      let show_cars = [...this.state.show];
      for (let i = 0; i < show_cars.length; i++) {
        show[i]= false;
      }
      this.setState({show})

      let show_car = {...this.state.show[car_id]};

      //show_car = this.state.show[car_id]? false : true;
      if (show[car_id] == false) {
        show_car = true;
        show[car_id]= show_car;
        this.setState({show});
      }

      this.setState({ open: !this.state.open });
      console.log(show);


    };


    toggle = () => {
        this.setState({ open: !this.state.open });
    };

    changeActiveItem = (activeItemIndex) => this.setState({ activeItemIndex });

    render() {
      let rows = [];
      let cars_show = [];
      this.props.data.forEach((dataObj) => {

        rows.push(
                <div className="card" onClick={() => this.handleClick(dataObj.id)}>

                  <img src={dataObj.image} className="image-size"/>
                  <div className="card-description">
                    <h2><span style={{fontSize: 40 }}>{((dataObj.price_cents * gon.date_count) + (gon.distance  * dataObj.price_by_km))}</span>€</h2>
                    <h2>{dataObj.name} </h2>
                    <p>{dataObj.year} , Moteur {dataObj.engine} ,  {dataObj.places} Places, {dataObj.gearbox} </p>

                  </div>
                  {/* <p className="crochet-right">{'>'}</p>
                  <div className="border-line"></div>  */}


                </div>

          );

        cars_show.push(

              <div>
                {
                  this.state.show[dataObj.id]?

                    <div className="car_info">
                        <h3>{dataObj.name} </h3>

                        <div className="carousel-pc box-show">
                          <ItemsCarousel
                            // Placeholder configurations
                            enablePlaceholder
                            numberOfPlaceholderItems={5}
                            minimumPlaceholderTime={1000}
                            placeholderItem={<div style={{ height: 400, background: '#900' }}>Placeholder</div>}

                            // Carousel configurations
                            numberOfCards={2}
                            gutter={12}
                            showSlither={true}
                            firstAndLastGutter={false}
                            freeScrolling={true}

                            // Active item configurations
                            requestToChangeActive={this.changeActiveItem}
                            activeItemIndex={this.state.activeItemIndex}
                            activePosition={'center'}

                            chevronWidth={24}
                            rightChevron={'>'}
                            leftChevron={'<'}
                            outsideChevron={true}
                          >

                               {dataObj.images.map((image) => {
                                 return (
                                  <div style={{ height: 400 }}>
                                      <img src={image} className="images-size" />
                                  </div>
                                 )
                                  })

                                }

                          </ItemsCarousel>
                        </div>
                        <div className="carousel-mobile box-show">
                          <ItemsCarousel
                            // Placeholder configurations
                            enablePlaceholder
                            numberOfPlaceholderItems={5}
                            minimumPlaceholderTime={1000}
                            placeholderItem={<div style={{ height: 200, background: '#900' }}>Placeholder</div>}

                            // Carousel configurations
                            numberOfCards={1}
                            gutter={12}
                            showSlither={true}
                            firstAndLastGutter={false}
                            freeScrolling={true}

                            // Active item configurations
                            requestToChangeActive={this.changeActiveItem}
                            activeItemIndex={this.state.activeItemIndex}
                            activePosition={'center'}

                            chevronWidth={24}
                            rightChevron={'>'}
                            leftChevron={'<'}
                            outsideChevron={false}
                          >

                               {dataObj.images.map((image) => {
                                 return (
                                  <div style={{ height: 200 }}>
                                      <img src={image} className="images-size" />
                                  </div>
                                 )
                                  })

                                }
                          </ItemsCarousel>
                        </div>
                        <div className="box-show">
                            <h4>Adresse</h4>
                            <p>{dataObj.address}</p>
                            <div id="map" className="map-style">
                            <GoogleMap
                              bootstrapURLKeys={gon.google_api_key}
                              center={[dataObj.latitude, dataObj.longitude]}
                              zoom={16}
                            >
                               <Pin
                              title={dataObj.name}
                              lat={dataObj.latitude}
                              lng={dataObj.longitude}
                              />
                            </GoogleMap>
                            </div>

                        </div>


                        <div className="box-show">

                            <h4>Caractéristiques techniques</h4>
                            <p>Moteur {dataObj.engine}</p>
                            <p>{dataObj.places} Places</p>
                            <p>Boite de Vitesse {dataObj.gearbox} </p>
                            <p>Consommation {dataObj.consommation} </p>

                        </div>
                        <div className="box-show">
                            <h4>Description</h4>
                            <p>{dataObj.description}</p>

                        </div>


                          <div className="d-flex text-center space-btn">
                              <a href={"rentals/new?car=" + dataObj.id + "&from=search_car"} className= 'btn btn-success'><p>Louer ce vehicule</p></a>
                          </div>


                    </div>

                    : null
                }
              </div>




          /*{ <TableRow
             key={dataObj.name}
             name={dataObj.name}
             price_cents={dataObj.price_cents} /> }
             <img src={`data:image/png;base64,${dataObj.image}`} className="image-size" />*/
        );
      });
      return (
        // <table>
        //   <thead>
        //     <tr>
        //       <th>name</th>
        //       <th>price_cents</th>
        //     </tr>
        //   </thead>
        //   <tbody>
        //     {rows}
        //   </tbody>
        // </table>
          <div>
            <div className="display-cars">
              {rows}
            </div>
            <Drawer
              open={this.state.open}
              onRequestClose={this.toggle}
              onDrag={() => {}}
              onOpen={() => {}}
              allowClose={true}
              modalElementClass="HugeList"
              containerElementClass="my-shade"
              parentElement={document.body} // element to be appended to
              direction="bottom"
            >
              <button className="button-close" onClick={this.toggle}>X</button>
              <div>
                {cars_show}
              </div>
            </Drawer>
          </div>

      )
    }
}


const PinStyles = {
    fontSize: '24px',
    color: 'red'
  }
  class Pin extends React.Component {
    render(){
      return(
        <div style={PinStyles}><FaMapPin /></div>
      )
    }
  }


export default Results;
