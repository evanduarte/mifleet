import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';




class Filter extends Component {
    constructor(props) {
      super(props);
      this.state = {
        status: [
          {id: 1, key:1, name: "A venir", value: "incoming",checked: true, className:"statusInComing"+this.props.proprio},
          {id: 2, key:2, name: "En cours", value: "in process",checked: false, className:"statusInProcess"+this.props.proprio},
          {id: 3, key:3, name: "Terminées", value: "finished",checked: false, className:"statusFinished"+this.props.proprio},
        ]
      };

    }  


    componentDidMount() {    
      document.getElementById(this.props.status_id).style.backgroundColor ="white"
      document.getElementById(this.props.status_id).style.borderRadius ="5px"
    }

    handleOnChange = (e) => {

      this.props.handleSearchEvents(e.target.value.toLowerCase(), e.target.name);
    };

                                                                                                                                                                                                                                                                                                                                                                                                                                     

    statusCounter = (value) => {
      let counter = 0;
    
      for (const input of this.props.data) {
        if (input.status === value) counter += 1;
      }
      return counter;
    };

    handleOnStatus = (name,value) => {
     
      let valArr = []
      let status = this.state.status
      status.forEach(statu => {
        if (statu.name === name) {
          statu.checked = true;
          this.setState({ checked: statu.checked })
          valArr.push(statu.value);
          document.getElementById(statu.className).style.backgroundColor ="white"
          document.getElementById(statu.className).style.borderRadius ="5px"
        } else {
          statu.checked = false;
          this.setState({ checked: statu.checked })
          document.getElementById(statu.className).style.backgroundColor =""
          document.getElementById(statu.className).style.borderRadius =""
        }
        
      })
     
      this.props.handleSearchEvents(valArr, "status");


      console.log(this.state.status)
    };

    
 
    render() {
        return (
     
            
            <div>
                <div>
                  <input      
                      placeholder={'Plaque...'}   
                      type="text"  
                      style={{textTransform: 'uppercase'}}
                      name='plaque'
                      value={this.props.plaque}  
                      onChange={this.handleOnChange} 
                      /> 
                </div> 
                
                <div className="status_search">
                  {
                    
                      this.state.status.map((statu) => {
                        return (
                          <div className="status_choice" id={statu.className} onClick={(e) => {this.handleOnStatus(statu.name,statu.value)}}>
                            <p className="status_text">{statu.name}{" ("}{this.statusCounter(statu.value)}{")"}</p>
                          </div> 
                      
                        );
                    
                  })
                  
                  }
                </div> 


                
            </div>
          
       
        )  
    }
}


export default Filter;