


import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';

import Filter from "./filter"
import Results from "./results"




const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
//toLocaleDateString('fr-FR', options)

class ResultsIndexRentals extends Component {
    constructor(props) {
      super(props);
      this.data = [];
      this.state = {
        plaque: '',
        status: [],
        status_id:"",
        proprio:""
      };
     }  
      
    componentWillMount() {       
        let array = []; 
        for (let i = 0; i < (gon.rentals_locataire).length; i++) {             
            let entry = {}; 
            entry.id = (gon.rentals_locataire)[i].id; 
            entry.starts_on = moment((gon.rentals_locataire)[i].starts_on).locale("fr").format('LLL');     
            entry.ends_on = moment((gon.rentals_locataire)[i].ends_on).locale("fr").format('LLL');
            entry.status = (gon.rentals_locataire)[i].status;
            entry.total_price_cents = (gon.rentals_locataire)[i].total_price_cents;
            entry.distance = (gon.rentals_locataire)[i].distance;
            entry.payment_status = (gon.rentals_locataire)[i].payment_status; 
            entry.name = (gon.cars_locataire)[i].name;
            entry.plaque = (gon.cars_locataire)[i].plaque.toLowerCase();
            entry.plaqueMaj = (gon.cars_locataire)[i].plaque;
            entry.address = (gon.cars_locataire)[i].address; 
            entry.image = (gon.car_image)[i];             
            entry.email = (gon.users_locataire)[i].email;
            entry.url = document.URL + "/" + (gon.rentals_locataire)[i].id
            entry.url_app = document.URL + "/" + (gon.rentals_locataire)[i].id
            array[i] = entry;  
            
        } 
        this.data = array;       
        this.setState({          
            isLoading: false,
            status: ["incoming"],
            status_id: "statusInComing",
            proprio: ""
        });
       
    }



    handleSearchEvents = (title, name) => {
        this.setState({ [name]: title });
    }


    render() {
        const filteredData = this.data.filter((dataObj)=>
          (dataObj.plaque.indexOf(this.state.plaque) !== -1)
          &&
          (    
            (dataObj.status.indexOf(this.state.status) !== -1)||
            (    
              (dataObj.status.indexOf(this.state.status[0]) !== -1)||
              (dataObj.status.indexOf(this.state.status[1]) !== -1)||
              (dataObj.status.indexOf(this.state.status[2]) !== -1)
            )          
          )
        )
     
        
        return (
            <div className="display_rental_page">
              <h5 className="title_my_rents">Gérer mes commandes</h5>
              <div className="display_filter_rental">
                <Filter 
                plaque={this.state.plaque}
                status={this.state.status}
                status_id={this.state.status_id}
                proprio={this.state.proprio}
                handleSearchEvents={this.handleSearchEvents}
                data={this.data}
                toggleFilter={this.toggleFilter}/>


              </div>

                <Results  
                plaque={this.state.plaque}
                status={this.state.status}
                data={this.data}
                data={filteredData}/>
            
            </div>
        )
    }
}







  document.addEventListener('turbolinks:load', () => {
    if (document.getElementById('rentals_results') != null) {

      ReactDOM.render(
        <ResultsIndexRentals />,
        document.getElementById('rentals_results'),
      )
    }
  });





  


