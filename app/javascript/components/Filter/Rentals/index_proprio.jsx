


import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';

import Filter from "./filter"
import Results from "./results"




const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
//toLocaleDateString('fr-FR', options)

class ResultsIndexMyRentals extends Component {
    constructor(props) {
      super(props);
      this.data = [];
      this.state = {
        plaque: '',
        status: [],
        status_id:"",
        proprio:""
      };
     }  
      
    componentWillMount() {       
        let array = []; 
        for (let i = 0; i < (gon.rentals_proprio).length; i++) {             
            let entry = {}; 
            entry.id = (gon.rentals_proprio)[i].id; 
            entry.starts_on = moment((gon.rentals_proprio)[i].starts_on).locale("fr").format('LLL');     
            entry.ends_on = moment((gon.rentals_proprio)[i].ends_on).locale("fr").format('LLL');
            entry.status = (gon.rentals_proprio)[i].status;
            entry.total_price_cents = (gon.rentals_proprio)[i].total_price_cents;
            entry.payment_status = (gon.rentals_proprio)[i].payment_status; 
            entry.distance = (gon.rentals_proprio)[i].distance; 
            entry.name = (gon.cars_proprio)[i].name;
            entry.plaque = (gon.cars_proprio)[i].plaque.toLowerCase();
            entry.plaqueMaj = (gon.cars_proprio)[i].plaque;
            entry.address = (gon.cars_proprio)[i].address; 
            entry.image = (gon.user_avatar)[i];             
            entry.email = (gon.users_proprio)[i].email;
            entry.url = "myrentals/" + (gon.rentals_proprio)[i].id
            entry.url_app = "myrentals_app/" + (gon.rentals_proprio)[i].id
            array[i] = entry;  
            
        } 
        this.data = array;       
        this.setState({          
            isLoading: false,
            status: ["incoming"],
            status_id:"statusInComingProprio",
            proprio: "Proprio"
        });
       
    }



    handleSearchEvents = (title, name) => {
        this.setState({ [name]: title });
    }


    render() {
        const filteredData = this.data.filter((dataObj)=>
          (dataObj.plaque.indexOf(this.state.plaque) !== -1)
          &&
          (    
            (dataObj.status.indexOf(this.state.status) !== -1)||
            (    
              (dataObj.status.indexOf(this.state.status[0]) !== -1)||
              (dataObj.status.indexOf(this.state.status[1]) !== -1)||
              (dataObj.status.indexOf(this.state.status[2]) !== -1)
            )          
          )
        )
     
        
        return (
            <div className="display_rental_page">
                <h5 className="title_my_rents">Gérer mes locations</h5> 
                <div className="display_filter_rental">
                    <Filter 
                    plaque={this.state.plaque}
                    status={this.state.status}
                    status_id={this.state.status_id}
                    proprio={this.state.proprio}
                    handleSearchEvents={this.handleSearchEvents}
                    data={this.data}
                    toggleFilter={this.toggleFilter}/>


                </div>

                    <Results  
                    plaque={this.state.plaque}
                    status={this.state.status}
                    data={this.data}
                    data={filteredData}/>
            
            </div>
        )
    }
}







  document.addEventListener('turbolinks:load', () => {
    if (document.getElementById('my_rentals_results') != null) {

      ReactDOM.render(
        <ResultsIndexMyRentals />,
        document.getElementById('my_rentals_results'),
      )
    }
  });





  


