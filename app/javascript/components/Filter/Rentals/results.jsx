

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';



class Results extends Component {  
    
    constructor(props) {
      super(props);
      this.state = {
        plaque:''
      }
    };
    




    render() {   
      let rows = []; 
    
      this.props.data.forEach((dataObj) => { 
        if (gon.controller_name === "rentals") {
        rows.push(

                <div className="rentals_card card">
                  
                  <img src={dataObj.image} className="image-size-rentals"/>
                  <div className="rentals_description">
                    <h2><span style={{fontSize: 20}}>{dataObj.email}</span></h2>
                    <h2>{dataObj.name} </h2>
                    <h2><span style={{fontSize: 15}}>{"Du "}{dataObj.starts_on}</span></h2> 
                    <h2><span style={{fontSize: 15}}>{"Au "}{dataObj.ends_on}</span></h2>
                   
                    <p>{dataObj.plaqueMaj} </p>
                    
                  </div>
                  <div className="rental_price">
                    <p>{"€"} {dataObj.total_price_cents}</p>    
                    {
                      dataObj.payment_status == false &&
                      <p className="payment_status_false">{"Non payée"}</p>     
                    }  
                    {
                      dataObj.payment_status == true &&
                      <p className="payment_status_true">{"Payée"}</p>     
                    }  
                  </div>
                  <a href={dataObj.url} className= 'card-link'></a>
                </div>


                
          );
        } else if (gon.controller_name === "rentals_app") {
          rows.push(
  
                  <div className="rentals_card card">
                    
                    <img src={dataObj.user_image} className="image-size-rentals"/>
                    <div className="rentals_description">
                      <h2><span style={{fontSize: 20}}>{dataObj.email}</span></h2>
                      <h2>{dataObj.name} </h2>
                      <h2><span style={{fontSize: 15}}>{"Du "}{dataObj.starts_on}</span></h2> 
                      <h2><span style={{fontSize: 15}}>{"Au "}{dataObj.ends_on}</span></h2>
                     
                      <p>{dataObj.plaqueMaj} </p>
                      
                    </div>
                    <div className="rental_price">
                      <p>{"€"} {dataObj.total_price_cents}</p>    
                      {
                        dataObj.payment_status == false &&
                        <p className="payment_status_false">{"Non payée"}</p>     
                      }  
                      {
                        dataObj.payment_status == true &&
                        <p className="payment_status_true">{"Payée"}</p>     
                      }  
                    </div>
                    <a href={dataObj.url_app} className= 'card-link'></a>
                  </div>
  
  
                  
            );
          }
        
 
      });     
      return (      

            <div className="display-my_cars">
              {rows}
            </div>
           

      ) 
    }
}
export default Results;
