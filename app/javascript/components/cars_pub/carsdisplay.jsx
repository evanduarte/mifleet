import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';
import TouchCarousel from 'react-touch-carousel'
import NonPassiveTouchTarget from './NonPassiveTouchTarget'
import {
  range, clamp, getTouchPosition,
  getTouchId, omit, modCursor
} from './utils'
import cx from "classnames"

const query = window.location.search.slice(1)
const enableLoop = /\bloop\b/.test(query)
const enableAutoplay = /\bautoplay\b/.test(query)
const cardSize = 395
const cardPadCount = enableLoop ? 3 : 0
const carouselWidth = clamp(window.innerWidth, 0, 960)


class CarsDisplay extends Component {  
    
    constructor(props) {
      super(props);
      this.state = {
        activeItemIndex: 0
      };
      this.renderCard = this.renderCard.bind(this);
      this.renderCardPrice = this.renderCardPrice.bind(this);
      this.CarouselContainer = this.CarouselContainer.bind(this);
    };

    CarouselContainer(props) {
      const {cursor, carouselState: {active, dragging}, ...rest} = props
      let current = -Math.round(cursor) % 3
      while (current < 0) {
        current += 3
      }
      // Put current card at center
      const translateX = (cursor - cardPadCount) * cardSize + (carouselWidth - cardSize) / 2
      return (
        <NonPassiveTouchTarget
          className={cx(
            'carousel-container',
            {
              'is-active': active,
              'is-dragging': dragging
            }
          )}
        >
          <NonPassiveTouchTarget
            className='carousel-track'
            style={{transform: `translate3d(${translateX}px, 0, 0)`}}
            {...rest}
          />
            <div className='carousel-pagination-wrapper'>
            <ol className='carousel-pagination'>
              {this.props.data_rented.map((_, index) => (
                <li
                  key={index}
                  className={current === index ? 'current' : ''}
                />
              ))}
            </ol>
          </div>
        </NonPassiveTouchTarget>
      )
    }
    
    
    renderCard (index, modIndex) {
      console.log("data rented", this.props.data_rented)
      const item = this.props.data_rented[modIndex]
      return (
        <div
          key={index}
          className='carousel-card'
        >
         <div className="card">    
          <img src={item.image} className="image-size"/>
          <div className="card-description">
            <h2><span style={{fontSize: 50}}>{item.price_cents}</span>€/Jrs</h2>
            <h2>{item.name} </h2>
            <p>{item.year} , Moteur {item.engine} ,  {item.places} Places, {item.gearbox} </p>
            
          </div>
          
          <a href={document.URL + "/" + item.id} className= 'card-link'></a>
        </div>
        </div>
      )
    }

    renderCardPrice (index, modIndex) {
      console.log("data low pirce", this.props.data_lowprice)
      const item = this.props.data_lowprice[modIndex]
      return (
        <div
          key={index}
          className='carousel-card'
        >
         <div className="card">    
          <img src={item.image} className="image-size"/>
          <div className="card-description">
            <h2><span style={{fontSize: 50}}>{item.price_cents}</span>€/Jrs</h2>
            <h2>{item.name} </h2>
            <p>{item.year} , Moteur {item.engine} ,  {item.places} Places, {item.gearbox} </p>
            
          </div>
          
          <a href={document.URL + "/" + item.id} className= 'card-link'></a>
        </div>
        </div>
      )
    }

    
    render() {   
      let rows = []; 
            
      this.props.data_rented.forEach((dataObj) => { 
          rows.push(
                  <div className="card">
                    
                    <img src={dataObj.image} className="image-size"/>
                    <div className="card-description">
                      <h2><span style={{fontSize: 50}}>{dataObj.price_cents}</span>€/Jrs</h2>
                      <h2>{dataObj.name} </h2>
                      <p>{dataObj.year} , Moteur {dataObj.engine} ,  {dataObj.places} Places, {dataObj.gearbox} </p>
                      
                    </div>
                    
                    <a href={document.URL + "/" + dataObj.id} className= 'card-link'></a>
                  </div>
                  
            );
      });   
      return (      
   
          <div>
            <h5 className="title_my_cars">Les plus louées</h5> 
            <TouchCarousel
              component={this.CarouselContainer}
              cardSize={cardSize}
              cardCount={this.props.data_rented.length}
              cardPadCount={cardPadCount}
              loop={false}
              autoplay={enableAutoplay ? 2e3 : false}
              renderCard={this.renderCard}
              onRest={index => console.log(`rest at index ${index}`)}
              onDragStart={() => console.log('dragStart')}
              onDragEnd={() => console.log('dragEnd')}
              onDragCancel={() => console.log('dragCancel')}
            />

            <h5 className="title_my_cars">Les moins chères</h5> 
            <TouchCarousel
              component={this.CarouselContainer}
              cardSize={cardSize}
              cardCount={this.props.data_rented.length}
              cardPadCount={cardPadCount}
              loop={false}
              autoplay={enableAutoplay ? 2e3 : false}
              renderCard={this.renderCardPrice}
              onRest={index => console.log(`rest at index ${index}`)}
              onDragStart={() => console.log('dragStart')}
              onDragEnd={() => console.log('dragEnd')}
              onDragCancel={() => console.log('dragCancel')}
            />

          </div>

      ) 
    }
}





export default CarsDisplay;