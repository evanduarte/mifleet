
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import styled from "styled-components"
import InputRange from 'react-input-range';
import Drawer from 'react-drag-drawer'
import 'react-input-range/lib/css/index.css';
import GoogleMap from 'google-map-react';
import { FaMapPin } from 'react-icons/fa';
import ItemsCarousel from 'react-items-carousel';
import TouchCarousel from 'react-touch-carousel'


import CarsDisplay from "./carsdisplay"






class ResultsCarsApp extends Component {
    constructor(props) {
      super(props);
      this.data = [];
      this.state = {
        name: '',
        type_car: [],
        gearbox: [],
        consommation: [],
        places: 9,
        isLoading: true,
        open: false,
        activeItemIndex: 0
      };
     }  
      
    componentWillMount() {       
        let array = []; 
        for (let i = 0; i < (gon.results_all_cars).length; i++) {             
            let entry = {}; 
            entry.id = (gon.results_all_cars)[i].id; 
            entry.user_id = (gon.results_all_cars)[i].user_id; 
            entry.name = (gon.results_all_cars)[i].name;     
            entry.year = (gon.results_all_cars)[i].year; 
            entry.engine = (gon.results_all_cars)[i].engine;    
            entry.places = (gon.results_all_cars)[i].places; 
            entry.gearbox = (gon.results_all_cars)[i].gearbox; 
            entry.consommation = (gon.results_all_cars)[i].consommation;  
            entry.price_cents = (gon.results_all_cars)[i].price_cents;          
            entry.price_by_km = (gon.results_all_cars)[i].price_by_km;    
            entry.type_car = (gon.results_all_cars)[i].type_car;                  
            entry.address = (gon.results_all_cars)[i].address;
            entry.description = (gon.results_all_cars)[i].description;
            entry.image = ((gon.results_image_all_cars)[i]);
            entry.most_rent = ((gon.most_rent)[i]);
            array[i] = entry;       
        } 
        this.data = array;       
        this.setState({          
            isLoading: false,
        }); 
        console.log("data this : ",this.data)
    }

 
    



    handleSearchEvents = (title, name) => {
        this.setState({ [name]: title });
    }



    render() {
        console.log("data cars :",this.data);
        const mostRented = this.data.sort((a,b) => {
          return b.most_rent - a.most_rent;
        });
        const threeMostRented = mostRented.slice(0, 3)

        const lowPrice = this.data.sort((a,b) => {
          return  a.price_cents - b.price_cents;
        });
        const threeLowPrice = lowPrice.slice(0, 3)

        
        
        return (
            <div className="display-page">
              
                <CarsDisplay  
                name={this.state.name}
                type_car={this.state.type_car}
                places={this.state.places}
                gearbox={this.state.gearbox}
                consommation={this.state.consommation}
                id={this.state.id}
                data={this.data}
                data_rented={threeMostRented}
                data_lowprice={threeLowPrice}
                />
            
            </div>
        )
    }
}







  document.addEventListener('turbolinks:load', () => {
    if (document.getElementById('cars_results_sign_out') != null) {

      ReactDOM.render(
        <ResultsCarsApp />,
        document.getElementById('cars_results_sign_out'),
      )
    }
  });