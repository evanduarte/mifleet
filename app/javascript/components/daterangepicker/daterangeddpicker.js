





$(function() {
    // var datesStart = $(".datepick").attr('data-start');
    // var datesEnd = $(".datepick").attr('data-end');
    // console.log("check date before");
    // console.log(datesStart);
    // console.log(datesEnd);
    // datesStart=datesStart.split(",")
    // datesEnd=datesEnd.split(",")
  
    // for (let pas = 0; pas < datesStart.length ; pas++) {
    //   datesStart[pas]=datesStart[pas].replace("[","").replace("]","")
    //   //datesStart[pas]=moment(datesStart[pas])
    //   datesEnd[pas]=datesEnd[pas].replace("[","").replace("]","")
    //   //datesEnd[pas]=moment(datesEnd[pas])
    // }
    
  
    // // $.get('articles/' + id, function(data) {
    // //   // Handle the result
    // //   $('.article-window').html(data);
    // // });
    // console.log("passing date from view to js");
    // console.log(datesStart);
    // console.log(datesEnd);

    if (gon.date_start === "") {
      gon.date_start=moment().startOf('hour');
    } else {  
      gon.date_start=moment(gon.date_start);
    }
    if (gon.date_end === "") {
      gon.date_end=moment().startOf('hour').add(24, 'hour');
    } else {
      gon.date_end=moment(gon.date_end);
    }


    let DatesInv = []
    const messagesDiv = document.getElementsByClassName("invalid-date-message");
    const btnRent = document.querySelector(".button-rent");
    const homeSearch = document.querySelectorAll("#search_date");
    var today = new Date();
    console.log(homeSearch)

    $(homeSearch).daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      minDate:today,
      startDate: moment().startOf('hour'),
      endDate: moment().startOf('hour').add(24, 'hour'),
      locale: {
        format: 'YYYY-MM-DD HH:mm:ss',
        //separator: " - ",
        daysOfWeek: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve','Sa'],
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        firstDay:1
      },
      opens: "center",
      autoApply: true,
      singleDatePicker: false,
    });
    
    $('input[name="datetimes"]').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      minDate:today,
      startDate: gon.date_start,
      endDate: gon.date_end,
      locale: {
        format: 'YYYY-MM-DD HH:mm:ss',
        //separator: " - ",
        daysOfWeek: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve','Sa'],
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        firstDay:1,
        applyLabel: "Appliquer",
        cancelLabel: "Annuler"
      },
      opens: "center",
      autoApply: true,
      singleDatePicker: false,
      isInvalidDate: function(date) {
        for (let pas = 0; pas < gon.invalidDates.length ; pas++) {
          if (moment(formatDate(date)) >= moment(formatDate(gon.invalidDates[pas].starts_on))  && moment(formatDate(date)) <= moment(formatDate(gon.invalidDates[pas].ends_on))) {
            DatesInv.push(date)
          }
        }
     
     
        
        if (DatesInv.includes(date)) {
          return true;
        }

      },
      isCustomDate: function(date) {
        if (DatesInv.includes(date)) {
          return "date-invalid";
        }
      }
    },

    function(start, end, check) {
      console.log("Callback has been called!");
      console.log(DatesInv)
      if (DatesInv.length != 0) {
        DatesInv.forEach(DateToCheck => {
          check = false;
          if (Date.parse(start) < Date.parse(DateToCheck) && Date.parse(end) > Date.parse(DateToCheck)) {
            var i;
            for (i = 0; i < messagesDiv.length; i++) {
              var classMessagesDiv = messagesDiv[i];
              if (classMessagesDiv.classList.contains('show')) {
                check = true;
              }
            }

            if (check === false) {
              classMessagesDiv.classList.add("show");
              btnRent.classList.add("opacity-btn-rent");
              $('.button-rent').attr('disabled', true);   
            }

          } else {
            var j;
            for (j = 0; j < messagesDiv.length; j++) {
              var classMessagesDiv = messagesDiv[j];
              if (classMessagesDiv.classList.contains('show')) {
                classMessagesDiv.classList.remove('show');
                
              }
              btnRent.classList.remove("opacity-btn-rent");
              $('.button-rent').attr('disabled', false); 
            }
          }
        })
      } else {
        btnRent.classList.remove("opacity-btn-rent");
        $('.button-rent').attr('disabled', false)
      }
     });

  


    function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }
   

  });







  // isInvalidDate: (function) A function that is passed each date in the two calendars 
  // before they are displayed, and may return true or false to indicate whether that date should be available for selection or not. 

  // isCustomDate: (function) A function that is passed each date in the two calendars before they are displayed, and may return a
  // string or array of CSS class names to apply to that date's calendar cell. 