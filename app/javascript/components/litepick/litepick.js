
var userAgent = window.navigator.userAgent.toLowerCase(),
    safari = /safari/.test( userAgent ),
    ios = /iphone|ipod|android|ipad/.test( userAgent );

 
if( ios ) {

  $('.timepicker').timepicker({ 'scrollDefault': 'now' });


  var date = new Date();
date.setDate(date.getDate() - 1);

const picker = new Litepicker({
    element: document.getElementById('date-starting'),
    elementEnd: document.getElementById('date-ending'),
    singleMode: true,
    allowRepick: false,
    numberOfColumns: 1,
    numberOfMonths: 1,
    lang: "fr-FR",
    showTooltip: true,
    splitView: false,
    position: "bottom left",
    switchingMonths: 1,
    minDate: date,
    startDate: moment(gon.start_date),
    endDate: moment(gon.end_date)
})

} else {

    var date = new Date();
date.setDate(date.getDate() - 1);

const picker = new Litepicker({
    element: document.getElementById('date-starting'),
    elementEnd: document.getElementById('date-ending'),
    singleMode: false,
    allowRepick: false,
    numberOfColumns: 2,
    numberOfMonths: 2,
    lang: "fr-FR",
    showTooltip: true,
    splitView: false,
    position: "bottom left",
    switchingMonths: 1,
    minDate: date,
    startDate: moment(gon.start_date),
    endDate: moment(gon.end_date)
})
};
