const messagesDiv = document.getElementsByClassName("invalid-date-message");
const btnRent = document.querySelector(".button-rent");


let DatesInv = []
for (let pas = 0; pas < gon.invalidDates.length ; pas++) {
    date_increment = moment(gon.invalidDates[pas].starts_on).set("hour",0).set('minute', 0).set('second', 0);

    while (date_increment < moment(gon.invalidDates[pas].ends_on)) {;
        DatesInv.push(date_increment.toDate().toString());
        date_increment.add(1, 'day');
    }
}

var date = new Date();
date.setDate(date.getDate() - 1);

const picker = new Litepicker({
    element: document.getElementById('rent-date-starting'),
    elementEnd: document.getElementById('rent-date-ending'),
    singleMode: false,
    allowRepick: false,
    numberOfColumns: 2,
    numberOfMonths: 2,
    lang: "fr-FR",
    showTooltip: true,
    splitView: false,
    position: "bottom",
    switchingMonths: 1,
    resetButton: true,
    minDate: date,
    startDate: moment(gon.date_start),
    endDate: moment(gon.date_end),
    // enable option to trigger check
    disallowLockDaysInRange: true,
    // check locked days yourself
    lockDaysFilter: (date1, date2, pickedDates) => {

        if (!date2) {
            if (DatesInv.includes(moment(date1.toJSDate()).toDate().toString())) {
                return true;    
            }
            return false;
        }
        

        while (date1.toJSDate() < date2.toJSDate()) {
            const day = moment(date1.toJSDate()).toDate().toString();
            isInRange = DatesInv.includes(day);
            if (isInRange) { return true; }
            date1.add(1, 'day');
        }
    
        return false;
    },
    setup: (picker) => {
        picker.on('error:range', () => {
            console.log("invalide range");
            check = false;
            invalidRange = false;
            var i;
            for (i = 0; i < messagesDiv.length; i++) {
                var classMessagesDiv = messagesDiv[i];
                if (classMessagesDiv.classList.contains('show')) {
                check = true;
                }
            }

            if (check === false) {
                console.log("message invalid");
                classMessagesDiv.classList.add("show");
                btnRent.classList.add("opacity-btn-rent");
                $('.button-rent').attr('disabled', true);  
                invalidRange = true; 
            }

        });

        picker.on('selected', (date1, date2) => {
            console.log("valid date");
            var j;
            for (j = 0; j < messagesDiv.length; j++) {
                var classMessagesDiv = messagesDiv[j];
                if (classMessagesDiv.classList.contains('show')) {
                    classMessagesDiv.classList.remove('show');
                    
                }
                btnRent.classList.remove("opacity-btn-rent");
                $('.button-rent').attr('disabled', false); 
            }
        });
    },


  })


  
  
