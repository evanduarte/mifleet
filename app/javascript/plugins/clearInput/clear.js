

function clear_form_elements(ele) {

    $(ele).find(':input').each(function() {
        
        switch(this.type) {
            case 'password':
            case 'text':
                $(this).val('');
                break;
        }
    });

}