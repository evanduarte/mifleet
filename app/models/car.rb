class Car < ApplicationRecord
    #mount_uploaders :cars, CarUploader
    #has_many :rentals, dependent: :destroy
    #reverse_geocoded_by :latitude, :longitude
    #after_validation :reverse_geocode  # auto-fetch address
    after_initialize :set_defaults, unless: :persisted?
    belongs_to :user
    has_many :rentals
    has_many_attached :car_images

    before_update :set_city
    after_commit :add_default_cars,on: %i[create update]

    validates :name, :plaque,:price_cents, :price_by_km, presence: true
    validates :price_cents, numericality: { only_integer: true, greater_than: 0 }, allow_blank: false
    validates :price_by_km, numericality: { greater_than: 0 }, allow_blank: false

    geocoded_by :address
    after_validation :geocode, if: :will_save_change_to_address?
    #validates :device_id, uniqueness: true

    def set_defaults
        self.statut  = "available"
        
    end

    def city_with_cars
      "Car #{cars} in #{city} "
    end

    def cars_thumbnail
        if car_images.attached?
            images = []
            car_images.each do |car_image|
                images << car_image.variant(resize_to_fill: [324, 240]).processed
               
            end
            return images            
        else
            "/default_car.jpg"
        end
    end

    def cars_thumbnail_show
        if car_images.attached?
            images = []
            car_images.each do |car_image|
                images << car_image.variant(resize_to_fill: [800, 500]).processed
               
            end
            return images            
        else
            "/default_car.jpg"
        end
    end

    def set_city
        results = Geocoder.search(address)
        if results.nil?
            result  = results.first.city
            self.city = result
        end
        
    end
    

    private

    def add_default_cars
        unless car_images.attached?
            car_images.attach(
            io: File.open(
                Rails.root.join(
                'app', 'assets', 'images', 'default_car.jpg'
                )
            ), 
            filename: 'default_car.jpg',
            content_type: 'image/jpg'
            )
        end
    end
    
  end