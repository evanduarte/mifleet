class Rental < ApplicationRecord

    after_initialize :set_defaults, unless: :persisted?
    belongs_to :user
    belongs_to :car
    validates :distance, presence: true
    validates :distance, numericality: { only_integer: true, greater_than: 10 }
    has_many :room_messages, dependent: :destroy,
                         inverse_of: :rental

    has_one_attached :front_left
    has_one_attached :front
    has_one_attached :front_right
    has_one_attached :left
    has_one_attached :right
    has_one_attached :rear_left
    has_one_attached :rear
    has_one_attached :rear_right
    has_many_attached :img_car_inside

    validates :front_left, :front, :front_right, :left, :right,:rear_left,:rear,:rear_right, presence: true, on: :update

    def set_defaults
        self.status  = "incoming"
        self.payment_status  = false

        #in process
        #finished
    end


    def img_car_inside_thumbnail
        if img_car_inside.attached?
            images = []
            img_car_inside.each do |car_image|
                images << car_image.variant(resize_to_fill: [250, 150]).processed

            end
            return images
        end
    end


end
