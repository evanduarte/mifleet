class RoomMessage < ApplicationRecord
  belongs_to :user
  belongs_to :rental, inverse_of: :room_messages

end
