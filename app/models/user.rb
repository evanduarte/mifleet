class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :verifications
  has_many :cars
  has_many :rentals
  has_one_attached :avatar
  after_commit :add_default_avatar, on: %i[create update]
  enum role: [:client, :renter,:moderator, :admin]
  enum license: [:not_verified, :manual, :verified], _prefix: :license
  enum cni: [:not_verified, :manual, :verified], _prefix: :cni
  geocoded_by :ip_address,
    :latitude => :lat, :longitude => :lon
  after_validation :geocode

  before_create :add_jti  
  
  after_initialize do
    if self.new_record?
      self.role ||= :client
      self.license ||= :not_verified
      self.cni ||= :not_verified
    end
  end

  def add_jti
    self.jti ||= SecureRandom.uuid
  end

  def avatar_thumbnail
    if avatar.attached?
      avatar.variant(resize: "150x150!").processed
    else
      "/default_profile.jpg"
    end
  end

  private

  def add_default_avatar
    unless avatar.attached?
      avatar.attach(
        io: File.open(
          Rails.root.join(
            'app', 'assets', 'images', 'default_profile.jpg'
          )
        ), 
        filename: 'default_profile.jpg',
        content_type: 'image/jpg'
      )
    end
  end


  
end
