class Verification < ApplicationRecord

    belongs_to :user
    # has_one_attached :imageFront
    # has_one_attached :imageBack

    enum status: [:await, :validated, :rejected]

    #validates :status, :type,:user_id, presence: true
    

    # def imageFront_thumbnail
    #     if imageFront.attached?              
    #         puts ("image front exist")
    #         imageFront = imageFront.variant(resize_to_fill: [324, 240]).processed
    #         return imageFront            
    #     else
    #         "/default_car.jpg"
    #     end
    # end


end