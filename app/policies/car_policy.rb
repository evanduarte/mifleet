class CarPolicy < ApplicationPolicy
  def index?
    true
  end

  def create?
    user.admin? || user.renter?
  end

  def update?
    return true if user.admin? || (user.renter? && user == car.user)
  end

  def destroy?
    return true if user.admin? || (user.renter? && user == car.user)
  end

  private

  def car
    record
  end
end
