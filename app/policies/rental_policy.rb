class RentalPolicy < ApplicationPolicy
    def index?
      true
    end
  
    def create?
      true
    end
  
    def update?
      return true if user.admin? || (user == rental.user)
    end
  
    def destroy?
      return true if user.admin? || (user == rental.user)
    end
  
    private
  
    def rental
      record
    end
  end