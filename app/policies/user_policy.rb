class UserPolicy < ApplicationPolicy
    def index?
        return true if user.admin? 
    end

    def mifleetcars?
        return true if user.admin? 
    end

    def update_car_device?
        return true if user.admin? 
    end

    def create_auth_code?
        return true if user.admin? 
    end

    def create_token?
        return true if user.admin? 
    end

    def create_token_second?
        return true if user.admin? 
    end
  
    def add_device_id?
        return true if user.admin? 
    end

  
    private
  

  end