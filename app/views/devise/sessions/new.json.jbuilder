if current_user.present?
    json.user do
        json.(current_user, :id, :email, :jti, :license, :cni)
    end
else
    json.user do
        json.jti nil
    end
end
