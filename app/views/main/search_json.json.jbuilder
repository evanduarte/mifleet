json.cars do
    json.array!(@cars) do |car|
        json.id car.id
        json.name car.name
        json.city car.city
        json.description car.description
        json.plaque car.plaque
        json.year car.year
        json.engine car.engine
        json.odometer car.odometer
        json.nb_door car.nb_door
        json.places car.places
        json.gearbox car.gearbox
        json.consommation car.consommation
        json.type_car car.type_car
        json.address car.address
        json.latitude car.latitude
        json.longitude car.longitude
        json.computerfueltank car.computerfueltank
        json.fueltank car.fueltank
        json.price_cents car.price_cents
        json.user_id car.user_id
        json.statut car.statut
        json.first_image car.first_image
    end
end

json.rentals do
    json.array!(@rentals) do |rental|
        json.id rental.id
        json.starts_on rental.starts_on
        json.ends_on rental.ends_on
        json.status rental.status
        json.user_id rental.user_id
        json.car_id rental.car_id
        json.distance rental.distance
  
    end
end

json.result_images do
    json.array!(@result_images) do |images|
        json.array!(images) do |image|
            json.image image
        end
    end
end

json.car_user do
    json.array!(@car_user) do |user| 
        json.email user.email
    end
end

json.car_user_avatar do
    json.array!(@car_user_avatar) do |avatar| 
        json.car_user_avatar avatar
    end
end




# json.rentals do
#     json.array!(@rentals) do |rental|
#         json.starts_on rental.starts_on
#         json.ends_on rental.ends_on
#         json.status rental.status
#         json.total_price_cents rental.total_price_cents
#         json.user_id rental.user_id
#         json.car_id rental.car_id
#         json.distance rental.distance
#     end
# end
