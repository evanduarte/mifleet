Warden::JWTAuth.configure do |config|
    config.secret = '864b3e099f78ffe9157d317cecb76b2fe74c016baef4dc352b8d870a3287a548b65691a5b3544b0685dbe375290e0f5e3b0ad5c318c4541d0360498b278fd997'
    config.dispatch_requests = [
                                 ['POST', %r{^/api/login$}],
                                 ['POST', %r{^/api/login.json$}]
                               ]
    config.revocation_requests = [
                                   ['DELETE', %r{^/api/logout$}],
                                   ['DELETE', %r{^/api/logout.json$}]
                                 ]
  end