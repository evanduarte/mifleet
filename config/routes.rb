Rails.application.routes.draw do

  resources :room_messages
  devise_for :users , :controllers => { registrations: 'edit_customs' }

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
    get '/users' => 'edit_customs#edit'
  end

  # Devise (login/logout) for HTML requests
  # devise_for :users, defaults: { format: :html },
  #                        path: '',
  #                  path_names: { sign_up: 'register' },
  #                 controllers: {
  #                   sessions: 'sessions',
  #                   registrations: 'edit_customs',
  #                   confirmations: 'confirmations'
  #                 }
  # devise_scope :user do
  #   get 'sign_in', to: 'devise/sessions#new'
  #   get 'register', to: 'devise/registrations#new'
  #   post 'register', to: 'devise/registrations#create'
  #   delete 'sign_out', to: 'devise/sessions#destroy'
  #   get 'confirmation/sent', to: 'confirmations#sent'
  #   get 'confirmation/:confirmation_token', to: 'confirmations#show'
  #   patch 'confirmation', to: 'confirmations#create'
  # end

  # API namespace, for JSON requests at /api/sign_[in|out]
  namespace :api do
    devise_for :users, defaults: { format: :json },
                     class_name: 'ApiUser',
                           skip: [:registrations, :invitations,
                                  :passwords, :confirmations,
                                  :unlocks],
                           path: '',
                     path_names: { sign_in: 'login',
                                  sign_out: 'logout' }
    devise_scope :user do
      get 'login', to: 'devise/sessions#new'
      delete 'logout', to: 'devise/sessions#destroy'
    end
  end

  root 'pages#home'

  resources :cars
  resources :rentals
  get '/search' => 'pages#search', :as => 'search_page'
  get :search_json, controller: :main

  get '/myrentals/:id', to: 'rentals#show_as_renter', as: "show_as_renter"

  resources :cars_app
  resources :rentals_app

  get '/myrentals_app/:id', to: 'rentals_app#show_as_renter', as: "show_as_renter_from_app"

  resources :charges
  #post '/authentication_tokens', to: "authentication_tokens#create"

  resources :room_messages


  resources :rentals_step
  
  get '/stepA/:id' => 'rentals_step#stepA', as: "stepA"
  get '/stepB/:id' => 'rentals_step#stepB', as: "stepB"
  get '/stepC/:id' => 'rentals_step#stepC', as: "stepC"
  get '/stepD/:id' => 'rentals_step#stepD', as: "stepD"
  get '/stepE/:id' => 'rentals_step#stepE', as: "stepE"
  post '/create_open_traccar' => 'rentals_step#create_open_traccar'


  resources :dashboard
  get '/gettoken' => 'dashboard#create_auth_code', :as => 'gettoken' #A transformer en post pour stocker token n°2 dans database
  get '/login_token' => 'dashboard#create_token', :as => 'login_token' #A transformer en post pour stocker token n°2 dans database
  get '/mifleetcars' => 'dashboard#mifleetcars', :as => 'mifleetcars'
  put '/update_car_device' => 'dashboard#update_car_device'

  get :customer, controller: :dashboard
  get '/customer/:id' => 'dashboard#show_customer', :as => 'getcustomer'
  get :vehicle, controller: :dashboard
  get :orders, controller: :dashboard
  get '/statistics'  => 'dashboard#statistics'

  get 'search_customer', to: 'dashboard#search_customer', defaults: { format: 'js' }
  get 'search_vehicle', to: 'dashboard#search_vehicle', defaults: { format: 'js' }
  get 'search_orders', to: 'dashboard#search_orders', defaults: { format: 'js' }
  get 'search_traccar', to: 'traccar#search_traccar', defaults: { format: 'js' }
  get 'search_traccar_connected', to: 'traccar#search_traccar_connected', defaults: { format: 'js' }


  resources :license
  resources :traccar
  get '/traccar_edit/:id' => 'traccar#traccar_edit', as: "traccar_edit"
  post '/traccar_command' => 'traccar#traccar_command'
  post '/add_traccar_device' => 'traccar#add_traccar_device'
  put '/add_device_id' => 'traccar#add_device_id'
  get '/traccar_api' => 'traccar#traccar_api'

  resources :verification
  #get :submit_verification, controller: :verification


end
