class FixColumnName < ActiveRecord::Migration[6.1]
  def change
    rename_column :cars, :type, :type_car
  end
end
