class AddGmapsToCars < ActiveRecord::Migration[6.1]
  def change
    add_column :cars, :gmaps, :string
  end
end
