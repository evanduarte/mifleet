class DeleteInfosFromRentals < ActiveRecord::Migration[6.1]
  def change
    remove_column :rentals, :car_id
    remove_column :rentals, :user_id
  end
end
