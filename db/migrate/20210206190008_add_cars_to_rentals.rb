class AddCarsToRentals < ActiveRecord::Migration[6.1]
  def change
    add_reference :rentals, :car, foreign_key: true
  end
end
