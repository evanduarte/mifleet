class AddDaterangeInRentals < ActiveRecord::Migration[6.1]
  def change
    add_column :rentals, :daterange, :string
  end
end
