class AddDistanceToRentals < ActiveRecord::Migration[6.1]
  def change
    add_column :rentals, :distance, :integer
  end
end
