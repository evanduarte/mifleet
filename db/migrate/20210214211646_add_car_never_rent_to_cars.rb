class AddCarNeverRentToCars < ActiveRecord::Migration[6.1]
  def change
    add_column :cars, :Car_never_rent, :boolean, default: false
  end
end
