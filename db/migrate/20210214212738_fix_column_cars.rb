class FixColumnCars < ActiveRecord::Migration[6.1]
  def change
    rename_column :cars, :Car_never_rent, :car_with_rents
  end
end
