class RemoveJwtBlacklist < ActiveRecord::Migration[6.1]
  def change
    drop_table :jwt_blacklist
  end
end
