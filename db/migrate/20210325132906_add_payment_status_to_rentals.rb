class AddPaymentStatusToRentals < ActiveRecord::Migration[6.1]
  def change
    add_column :rentals, :payment_status, :boolean
  end
end
