class AddRoomToRental < ActiveRecord::Migration[6.1]
  def change
    add_column :rentals, :room, :string, unique: true
  end
end
