class AddXeeinfoToCars < ActiveRecord::Migration[6.1]
  def change
    add_column :cars, :XeeClientKey, :string
    add_column :cars, :XeeClientSecret, :string
    add_column :cars, :XeeAccessTokken, :string
  end
end
