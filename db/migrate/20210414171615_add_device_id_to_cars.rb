class AddDeviceIdToCars < ActiveRecord::Migration[6.1]
  def change
      add_column :cars, :device_id, :string
  end
end
