class AddLicenseToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :license, :integer
  end
end
