class AddIndexToDeviceId < ActiveRecord::Migration[6.1]
  def change
    remove_column :cars, :device_id
    add_column :cars, :device_id, :string
    add_index  :cars, :device_id, unique: true
  end
end
