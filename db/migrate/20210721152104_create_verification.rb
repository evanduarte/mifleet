class CreateVerification < ActiveRecord::Migration[6.1]
  def change
    create_table :verifications do |t|
      t.integer :status
      t.string :type
      t.timestamps
    end
  end
end


