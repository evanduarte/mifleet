class AddRefToVerif < ActiveRecord::Migration[6.1]
  def change
    add_reference :verifications, :user, foreign_key: true
  end
end
