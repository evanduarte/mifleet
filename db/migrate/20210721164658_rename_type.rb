class RenameType < ActiveRecord::Migration[6.1]
  def change
    rename_column :verifications, :type, :class
  end
end
