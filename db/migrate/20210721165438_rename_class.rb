class RenameClass < ActiveRecord::Migration[6.1]
  def change
    rename_column :verifications, :class, :identity_type
  end
end
