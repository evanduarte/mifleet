class AddDataToVerif < ActiveRecord::Migration[6.1]
  def change
    add_column :verifications, :imageFront, :string
    add_column :verifications, :imageBack, :string
  end
end
