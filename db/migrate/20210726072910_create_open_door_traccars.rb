class CreateOpenDoorTraccars < ActiveRecord::Migration[6.1]
  def change
    create_table :open_door_traccars do |t|
      t.boolean :door_open
      t.timestamps
    end
  end
end
