class AddRefToOpenDoorTraccar < ActiveRecord::Migration[6.1]
  def change
    add_reference :open_door_traccars, :user, foreign_key: true
    add_reference :open_door_traccars, :car, foreign_key: true
    add_reference :open_door_traccars, :rental, foreign_key: true
  end
end
