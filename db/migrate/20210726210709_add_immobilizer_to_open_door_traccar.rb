class AddImmobilizerToOpenDoorTraccar < ActiveRecord::Migration[6.1]
  def change
    add_column :open_door_traccars, :immobilizer, :boolean
  end
end
