# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_26_210709) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "cars", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "plaque"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.json "cars"
    t.integer "year"
    t.string "engine"
    t.string "odometer"
    t.string "nb_door"
    t.integer "places"
    t.string "gearbox"
    t.string "consommation"
    t.string "type_car"
    t.decimal "price_by_km"
    t.string "address"
    t.string "city"
    t.float "latitude"
    t.float "longitude"
    t.float "computerfueltank"
    t.float "fueltank"
    t.string "gmaps"
    t.integer "price_cents", default: 0, null: false
    t.string "price_currency", default: "EUR", null: false
    t.bigint "user_id"
    t.string "statut"
    t.string "first_image"
    t.string "XeeClientKey"
    t.string "XeeClientSecret"
    t.string "XeeAccessTokken"
    t.string "society"
    t.string "device_id"
    t.index ["device_id"], name: "index_cars_on_device_id", unique: true
    t.index ["user_id"], name: "index_cars_on_user_id"
  end

  create_table "open_door_traccars", force: :cascade do |t|
    t.boolean "door_open"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.bigint "car_id"
    t.bigint "rental_id"
    t.boolean "immobilizer"
    t.index ["car_id"], name: "index_open_door_traccars_on_car_id"
    t.index ["rental_id"], name: "index_open_door_traccars_on_rental_id"
    t.index ["user_id"], name: "index_open_door_traccars_on_user_id"
  end

  create_table "rentals", force: :cascade do |t|
    t.datetime "starts_on"
    t.datetime "ends_on"
    t.string "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "total_price_cents", default: 0, null: false
    t.string "total_price_currency", default: "EUR", null: false
    t.bigint "user_id"
    t.bigint "car_id"
    t.string "daterange"
    t.integer "distance"
    t.boolean "payment_status"
    t.string "room"
    t.index ["car_id"], name: "index_rentals_on_car_id"
    t.index ["user_id"], name: "index_rentals_on_user_id"
  end

  create_table "room_messages", force: :cascade do |t|
    t.bigint "rental_id", null: false
    t.bigint "user_id", null: false
    t.text "message"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "color"
    t.index ["rental_id"], name: "index_room_messages_on_rental_id"
    t.index ["user_id"], name: "index_room_messages_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "role"
    t.float "ip_address"
    t.string "jti", null: false
    t.string "name"
    t.string "firstname"
    t.integer "phone"
    t.integer "license"
    t.integer "cni"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["jti"], name: "index_users_on_jti", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "verifications", force: :cascade do |t|
    t.integer "status"
    t.string "identity_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.string "imageFront"
    t.string "imageBack"
    t.index ["user_id"], name: "index_verifications_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "cars", "users"
  add_foreign_key "open_door_traccars", "cars"
  add_foreign_key "open_door_traccars", "rentals"
  add_foreign_key "open_door_traccars", "users"
  add_foreign_key "rentals", "cars"
  add_foreign_key "rentals", "users"
  add_foreign_key "room_messages", "rentals"
  add_foreign_key "room_messages", "users"
  add_foreign_key "verifications", "users"
end
